#ifndef Gramatica_H_
#define Gramatica_H_

#include <string>
#include <vector>

#include "Automat.h"

class Gramatica
{
private:
    //simboluri neterminale
    std::vector<char> N_;
    //alfabetul automatului
    std::vector<char> sigma_;
    //simbol de start
    char S_;
    //reguli de productie
    std::vector<std::string> P_;
public:
    //afiseaza gramatica la stdout
    void afisare();
    //seteaza membriii gramaticii pe baza
    //automatului a
    void transformaDinAutomat(Automat& a);
};
#endif
