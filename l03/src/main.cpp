#include "Automat.h"

#include <iostream>
#include <set>

using std::string;
using std::vector;
using std::map;
using std::pair;
using std::cout;
using std::set;

//realizeaza epsilon inchiderea unei multimi,
//intoarce reuniunea EI elementelor din multime
//EI -> espilon inchidere
vector<int> EImultime(vector<int>& stari, Automat& automat)
{
    vector<vector<int> > toateEI;
    FunctieDeTranzitie& delta = automat.delta();
    for (unsigned int idxStare = 0;
         idxStare < stari.size();
         ++idxStare)
    {
        toateEI.push_back(delta.epsilonInchidere(stari[idxStare]));
    }

    return reuniuneVectori(toateEI);
}

//realizeaza toate tranzitiile posibile
//pentru o multime de stari
//intoarce reuniunea rezultatelor
vector<int> tranzitieMultime(vector<int>& stari,
                             char simbol,
                             Automat& automat)
{
    vector<vector<int> > toateTranzitiile;
    FunctieDeTranzitie& delta = automat.delta();

    for (unsigned int idxStare = 0;
         idxStare < stari.size();
         ++idxStare)
    {
        toateTranzitiile.push_back(delta.efectueazaTranzitieMultime(stari[idxStare], simbol));
    }

    return reuniuneVectori(toateTranzitiile);
}

bool intersectieVectori(vector<int>& v1, vector<int>& v2)
{
    for (unsigned int idxV1 = 0;
         idxV1 < v1.size();
         ++idxV1)
    {
        for (unsigned int idxV2 = 0;
             idxV2 < v2.size();
             ++idxV2)
        {
            if (v1[idxV1] == v2[idxV2])
                return true;
        }
    }

    return false;
}

//converteste automatul finit nedeterminist
//automat intr-unul determinist
Automat problema1(Automat& automat)
{
    Automat rezultat;
    //rezultat are acelasi alfabet
    //cu automatul initial
    vector<char> sigma = automat.sigma();
    for (unsigned int idxSigma = 0;
         idxSigma < sigma.size();
         ++idxSigma)
    {
        if (sigma[idxSigma] != 'E')
        {
            rezultat.sigma().push_back(sigma[idxSigma]);
        }
    }
    FunctieDeTranzitie delta = automat.delta();
    
    //stucturi de date ajutatoare

    //set ce memoreaza EI existente
    //EI -> epsilon inchidere
    //folosit pt evitarea starilor compuse duplicat
    set<int> setEI;
    //pastreaza EI in forma extinsa;
    //algoritmul va lucra pe acest vector
    vector<vector<int> > vectorEI;
    //index al EI careia se
    //lucreaza la iteratia curenta
    unsigned int idxEI = 0;

    //generam EI pentru tranzitia initiala
    vector<int> EIStareInitiala = delta.epsilonInchidere(automat.q0());
    vectorEI.push_back(EIStareInitiala);
    setEI.insert(stareCompusa(EIStareInitiala));
    //salvam EI ca simbol de start pentru
    //automatul rezultat
    rezultat.q0(stareCompusa(EIStareInitiala));
    //adaugam EI la lista de stari a
    //automatului rezultat
    rezultat.adaugaStare(stareCompusa(EIStareInitiala));
    
    while (idxEI < vectorEI.size())
    {
        vector<int> EIcurenta = vectorEI[idxEI];
        
        //pentru fiecare simbol
        //realizam tranzitia EI curente
        vector<char>& simboluri = rezultat.sigma();
        for (unsigned int idxSimbol = 0;
             idxSimbol < simboluri.size();
             ++idxSimbol)
        {
            vector<int> tranzitieEI 
            = tranzitieMultime(EIcurenta, simboluri[idxSimbol], automat);
            //realizam EI a tranzitiei
            //si verificam daca face parte
            //din set
            vector<int> EIcandidat 
                = EImultime(tranzitieEI, automat);
            const int stareCompusaEI = stareCompusa(EIcandidat);
            if (stareCompusaEI != -1)
            {
                //adaugam tranzitia gasita la automat
                Tranzitie t(stareCompusa(EIcurenta),
                            simboluri[idxSimbol],
                            stareCompusaEI);
                rezultat.adaugaTranzitie(t);

                if (setEI.find(stareCompusaEI) == setEI.end())
                {
                    //daca nu face parte din set,
                    //adauga EIcandidat in automatul rezultat,
                    //in vectorEI si setEI
                    rezultat.adaugaStare(stareCompusaEI);
                    vectorEI.push_back(EIcandidat);
                    setEI.insert(stareCompusaEI);
                    
                    //daca EIcandiat contine o stare finala
                    //adaugam EIcandidat la multimea starilor
                    //finale a automatului rezultat
                    if (intersectieVectori(automat.f(), EIcandidat))
                    {
                        rezultat.f().push_back(stareCompusaEI);
                    }

                }
            }
        }
        //procesam urmatoarea EI
        //din vector
        ++idxEI;
    }

    return rezultat;
}

int main()
{
    Automat a;
    a.citesteDinFisier("input.txt");
    Automat a2 = problema1(a);
    a2.afisare();
    return 0;
}
