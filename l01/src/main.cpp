#include "Automat.h"

#include <iostream>
#include <string>
#include <map>
#include <set>

using std::string;
using std::vector;
using std::map;
using std::set;
using std::pair;

bool limbajRecunoscut(string cuvant, Automat& automat)
{
    FunctieDeTranzitie& f = automat.delta();
    const unsigned int dimCuvant = cuvant.size();
    int stare= automat.q0();

    for (unsigned int charIdx = 0; charIdx < dimCuvant; ++charIdx)
    {
       //se efectueaza cate o tranzitie pentru fiecare
       //simbol(litera) a cuvantului
       //daca se ajunge intr-o stare invalida(nu se poate
       //face tranzitie), atunci se returneaza false 
       stare = f.efectueazaTranzitie(stare, cuvant[charIdx]); 
       if (stare == -1)
           return false;
    }

    //se verifica daca starea in care am ajuns dupa efectuarea
    //tututor tranzitiilor face parte din multimea starilor
    //finale
    vector<int>& stariFinale = automat.f();
    const unsigned int dimStariFinale = stariFinale.size();

    for (unsigned int stariFinaleIdx = 0; 
         stariFinaleIdx < dimStariFinale;
         ++stariFinaleIdx)
    {
        if (stare == stariFinale[stariFinaleIdx])
            return true;
    }

    //daca starea finala in care am ajuns nu se gaseste
    //in multimea starilor finale, se returneaza false
    return false;

}

string infinit()
{
    string s;
    s += static_cast<char>(255);
    return s;
}

//obiect cu ajutorul caruia
//vom tine starile in ordine
//pe baza drumului pana la ele,
//in ordine crescatoare
struct Comparator
{
    //referinta la map-ul din functia de
    //mai jos
    //folosim referinta, pentru a primi
    //modificarile facute pe parcursul
    //algoritmului
    map<int, string>& distanta;
    //constructor
    Comparator(map<int, string>& d)
        :distanta(d)
    {}
    //functie care compara doua stari pe
    //baza distantei
    //intoarce true daca stare1 are distanta
    //mai mica si false in caz contrar
    bool operator()(int stare1, int stare2)
    {
        //returnez valoarea intoarsa de operatorul <
        return (distanta[stare1] < distanta[stare2]);
    }
};

bool conditieDeStop(map<int, string>& distanta,
                    set<int, Comparator>& stariNevizitate)
{
    //daca setul e vid se intoarce true
    if (stariNevizitate.begin() ==
        stariNevizitate.end())
    {
        return true;
    }

    //se verifica daca prima stare din cele nevizitate
    //are distanta infinit
    const int primaStareNevizitata = *(stariNevizitate.begin());
    if (distanta[primaStareNevizitata] == 
        infinit())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//se actualizeaza starile nevizitate
//prin stergerea si adaugarea starii modificate
//in acest fel se reorganizeaza setul
void actualizeazaStariNevizitate
     (set<int, Comparator>& stariNevizitate,
      int stareModificata)
{
    stariNevizitate.erase(stareModificata);
    stariNevizitate.insert(stareModificata);
}

//calculeaza cel mai scurt drum de la stareaInitiala la cea finala
//daca nu exista drum, atunci va returna litera '255'
//daca starile coincid, atunci va returna cuvantul vid ""
string Dijkstra(Automat& automat, int stareInitiala, int stareFinala)
{
    //INSPIRATIE: 
    //https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm 
    
    //map ce asociaza fiecarei stari din automat o distanta
    map<int, string> distanta;
    //set care memoreaza starile nevizitate in ordine crescatoare,
    //pe baza distantei pana la ele
    Comparator comparator(distanta);
    set<int, Comparator> stariNevizitate(comparator);

    //initializare structuri de date
    distanta[stareInitiala] = "";
    stariNevizitate.insert(stareInitiala);
    std::vector<int>& stari = automat.q();
    for (unsigned int stareIdx = 0; 
         stareIdx < stari.size(); 
         ++stareIdx)
    {
        if (stari[stareIdx] != stareInitiala)
        {
            //asociem fiecarei stari valoarea infinit pentru
            //distanta
            distanta[ stari[stareIdx] ] = infinit();
            //adaugam toate starile, in afara de cea initiala
            //in stariNevizitate
            stariNevizitate.insert(stari[stareIdx]);
        }
    }

    int stareCurenta = stareInitiala;
    bool exit = false;
    while (!exit)
    {
        //luam toate starile in care putem
        //ajunge din starea curenta
        vector<pair<char, int> > tranzitii =
            automat.delta().gasesteTranzitii(stareCurenta);
        //parcurg toate tranzitiile vecine
        vector<pair<char, int> >::iterator itTranzitie;
        for (itTranzitie = tranzitii.begin();
             itTranzitie != tranzitii.end();
             ++itTranzitie)
        {
            //calcul distanta tentativa
            string distantaTentativa = distanta[stareCurenta];
            distantaTentativa += (*itTranzitie).first;
            //daca distanta tentativa este mai mica decat
            //cea din map se actualizeaza distanta
            if (distanta[(*itTranzitie).second] >
                distantaTentativa)
            {
                distanta[(*itTranzitie).second] =
                    distantaTentativa;

                actualizeazaStariNevizitate
                (stariNevizitate,
                 (*itTranzitie).second);
            }
        }
        //scot starea curenta din stariNevizitate
        stariNevizitate.erase(stareCurenta);
        exit = conditieDeStop(distanta, stariNevizitate);
        //se alege ca stare curenta prima stare din set
        if (!exit)
        {
            stareCurenta = *(stariNevizitate.begin());
        }
    }

    return distanta[stareFinala];
}
int main()
{
    Automat a;
    a.citesteDinFisier("input.txt");

    std::vector<int>& q = a.q();
    for (unsigned int i = 0; i < q.size(); ++i)
        std::cout << q[i] << " ";
    std::cout << "\n";

    std::vector<char>& sigma = a.sigma();
    for (unsigned int i = 0; i < sigma.size(); ++i)
    {
        std::cout << sigma[i] << " ";
    }
    std::cout << "\n";

    std::cout << a.q0() << "\n";

    std::vector<int>& f = a.f();
    for (unsigned int i = 0; i < f.size(); ++i)
    {
        std::cout << f[i] << " ";
    }
    std::cout << "\n";
    
    std::vector<Tranzitie>& delta = a.delta().tranzitii();
    for (unsigned int i = 0; i < delta.size(); ++i)
    {
        std::cout << delta[i].stareInitiala << " ";
        std::cout << delta[i].simbol << " ";
        std::cout << delta[i].stareFinala << "\n";
    }
    std::cout << "\n";
    
    string cuvant;
    //problema1
    std::cout << "Introduceti cuvant: ";
    std::cin >> cuvant;
    bool cuvantEsteAcceptat = limbajRecunoscut(cuvant, a);
    std::cout << "Cuvantul este acceptat: "
              << cuvantEsteAcceptat << "\n";
    //problema 2
    std::cout<< "Starile accesibile sunt: \n";
    for (unsigned int i = 0; i < q.size(); ++i)
    {
        if (q[i] != a.q0())
        {
            string drum = Dijkstra(a, a.q0(), q[i]);
            if (drum != infinit())
            {
                std::cout << q[i] << " ";
            }
        }
    }
    std::cout << "\n";
    std::cout<< "Starile finalizate sunt: \n";
    for (unsigned int i = 0; i < q.size(); ++i)
    {
        string drum = Dijkstra(a, q[i], f[0]);
        if (drum != infinit())
        {
            std::cout << q[i] << " ";
        }
    }
   
    std::cout<<"\n";
    return 0;
}
