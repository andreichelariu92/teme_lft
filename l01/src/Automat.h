#ifndef Automat_H_INCLUDE_GUARD
#define Automat_H_INCLUDE_GUARD

#include <vector>
#include <string>
#include <utility>

///structura ce defineste o
///tranzitie din starea initiala
///in starea finala folosind
///simbolul "simbol"
struct Tranzitie
{
    int stareInitiala;
    char simbol;
    int stareFinala;

    Tranzitie(int sI, char s, int sF)
        :stareInitiala(sI),
         simbol(s),
         stareFinala(sF)
    {}

    Tranzitie()
        :stareInitiala(0),
         simbol(0),
         stareFinala(0)
    {}
};

///clasa care defineste matricea de
///tranzitie a unui automat finit determinist
///ofera in plus metode ajutatoare
class FunctieDeTranzitie
{
    private:
        std::vector<Tranzitie> tranzitii_;
    public:
        FunctieDeTranzitie();
        FunctieDeTranzitie(std::vector<Tranzitie> tranzitii);
        //efectueaza o tranzitie din starea "stare" cu
        //ajutorul simbolului simbol
        //intoarce noua stare dupa tranzitie sau -1
        //daca nu se poate face tranzitia
        int efectueazaTranzitie(int stare, char simbol);
        ///intoarce un vector cu posibilele tranzitii
        ///din starea "stare"
        ///daca nu exista tranzitii,
        ///vectorul returnat va avea dimensiune 0
        std::vector<std::pair<char, int> > gasesteTranzitii(int stare);
        ///metoda de acces la tranizii
        std::vector<Tranzitie>& tranzitii()
        {
            return tranzitii_;
        }
};

///clasa ce defineste un automat
///finit determinist
class Automat
{
private:
    //multimea starilor
    std::vector<int> q_;
    //multimea simbolurilor
    std::vector<char> sigma_;
    //starea initiala
    int q0_;
    //multimea starilor finale
    std::vector<int> f_;
    //functie de tranzitie,
    //definita prin vector de tranzitii
    FunctieDeTranzitie delta_;
public:
    ///constructorul, operatorul= si
    ///destructorul sunt generati implicit
    ///de compilator
    
    ///se presupune ca fisierul e scris corect :)
    ///daca nu, atunci comportamentul functiei
    ///e nedefinit
    void citesteDinFisier(std::string fileName);

    ///metode de acces la variabilele automatului
    std::vector<int>& q()
    {
        return q_;
    }
    std::vector<char>& sigma()
    {
        return sigma_;
    }
    int q0()
    {
        return q0_;
    }
    std::vector<int>& f()
    {
        return f_;
    }
    FunctieDeTranzitie& delta()
    {
        return delta_;
    }


};
#endif
