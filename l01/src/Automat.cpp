#include "Automat.h"

#include <fstream>
#include <iostream>

using std::vector;
using std::pair;

FunctieDeTranzitie::FunctieDeTranzitie()
    :tranzitii_()
{}

FunctieDeTranzitie::FunctieDeTranzitie
                    (vector<Tranzitie> tranzitii)
    :tranzitii_(tranzitii)
{}

int FunctieDeTranzitie::efectueazaTranzitie
                        (int stare, char simbol)
{
    vector<Tranzitie>::iterator it;
    //stareRezultat este -1, marcata
    //ca nevalida, daca se va gasi o stare
    //in vector pentru care se va putea face
    //tranzitia, valoarea lui stareRezultat
    //va fi actualizata
    int stareRezultat = -1;
    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare &&
            (*it).simbol == simbol)
        {
            stareRezultat = (*it).stareFinala;
        }
    }

    return stareRezultat;
}

vector<pair<char, int> > FunctieDeTranzitie::gasesteTranzitii
                         (int stare)
{
    vector<pair<char, int> > tranzitiiPosibile;
    vector<Tranzitie>::iterator it;

    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare)
        {
            pair<char, int> p((*it).simbol, (*it).stareFinala);
            tranzitiiPosibile.push_back(p);
        }
    }

    return tranzitiiPosibile;
}
void Automat::citesteDinFisier(std::string fileName)
{
    std::ifstream f(fileName.c_str(), std::ifstream::in);

    //citire stari
    int nrStari = 0;
    f >> nrStari;
    q_.resize(nrStari);
    for (int stareIdx = 0; stareIdx < nrStari; ++stareIdx)
        f >> q_[stareIdx];
    //citire simboluri
    int nrSimboluri = 0;
    f >> nrSimboluri;
    sigma_.resize(nrSimboluri);
    for (unsigned int simbolIdx = 0; 
         simbolIdx < nrSimboluri; 
         ++simbolIdx)
    {
        f >> sigma_[simbolIdx];
    }
    //citire stare initiala
    f >> q0_;
    //citire stari finale
    int nrStariFinale = 0;
    f >> nrStariFinale;
    f_.resize(nrStariFinale);
    for (unsigned int stareFIdx = 0; 
         stareFIdx < nrStariFinale; 
         ++stareFIdx)
    {
        f >> f_[stareFIdx];
    }
    //citire functie tranzitie
    int stareInitiala;
    char simbol;
    int stareFinala;
    vector<Tranzitie> tranzitii;
    while (f >> stareInitiala >> simbol >> stareFinala)
    {
        Tranzitie t(stareInitiala, simbol, stareFinala);
        tranzitii.push_back(t);
    }
    delta_ = FunctieDeTranzitie(tranzitii);
}
