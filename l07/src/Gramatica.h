#ifndef Gramatica_H_INCLUDE_GUARD
#define Gramatica_H_INCLUDE_GUARD

#include <string>
#include <vector>
#include <istream>

///Structura ce reprezinta o regula de
///productie.
///Fiecare simbol este reprezentat ca un
///sir de caractere
///In simbolul de stop se retine un vector
///cu toate combinatiile de simboluri in care
///se ajunge din simbolul de start
///De exemplu: A->a si A -> BC
///atunci vom avea dreapta = {{a}, {B, C}}
typedef std::vector<std::vector<std::string> > MatriceString_t;
class Productie
{
private:
    std::string stanga_;
    MatriceString_t dreapta_;
    friend std::istream& operator >> (std::istream& is, Productie& p);
public:
    Productie()
        :stanga_(),
         dreapta_()
    {}
    
    Productie(std::string stanga, MatriceString_t dreapta)
        :stanga_(stanga),
         dreapta_(dreapta)
    {}
             
    template<typename Functor>
    MatriceString_t filtreazaDreapta(Functor f)
    {
        MatriceString_t output;
        MatriceString_t::iterator vectorIt;
        for (vectorIt = dreapta_.begin();
             vectorIt != dreapta_.end();
             ++vectorIt)
        {
            if (f(stanga_, *vectorIt))
            {
                output.push_back(*vectorIt);
            }
        }

        return output;
    }

    void adaugaDreapta(std::vector<std::string>& v)
    {
        dreapta_.push_back(v);
    }
    
    std::string stanga()
    {
        return stanga_;
    }

    MatriceString_t& dreapta()
    {
        return dreapta_;
    }

    void stanga(std::string stanga)
    {
        stanga_ = stanga;
    }

    void dreapta(MatriceString_t dreapta)
    {
        dreapta_ = dreapta;
    }
};

///supraincarcarea operatorului >> pentru citirea
///mai usoara din fisier a gramaticii
std::istream& operator >> (std::istream& is, Productie& p);

///Clasa ce cuprinde mai multe reguli de productie
///ofera operatii ajutatoare
class Productii
{
private:
    std::vector<Productie> productii_;
public:
    Productii(const std::vector<Productie>& productii)
        :productii_(productii)
    {}
    
    Productii()
        :productii_()
    {}

    void adaugaProductie(const Productie& p);

    std::vector<Productie> productii()const
    {
        return productii_;
    }
};

///Clasa ce reprezinta o gramatica independenta
///de context(cred)
class Gramatica
{
private:
    std::vector<std::string> N_;
    std::vector<std::string> sigma_;
    Productii P_;
    std::vector<std::string> S_;
public:
    Gramatica(std::vector<std::string> N,
              std::vector<std::string> sigma,
              Productii P,
              std::vector<std::string> S)
        :N_(N),
         sigma_(sigma),
         P_(P),
         S_(S)
    {}

    std::vector<std::string> N()const
    {
        return N_;
    }
    void N(const std::vector<std::string>& n)
    {
        N_ = n;
    }

    std::vector<std::string> sigma()const
    {
        return sigma_;
    }
    void sigma(const std::vector<std::string>& s)
    {
        sigma_ = s;
    }

    Productii P()const
    {
        return P_;
    }
    void P(const Productii& p)
    {
        P_ = p;
    }
    
    std::vector<std::string> S()const
    {
        return S_;
    }
    void S(const std::vector<std::string>& s)
    {
        S_ = s;
    }

    void afisare();

};

///functie care intoarce true daca simbol
///este terminal (contine doar litere mici)
bool terminal(std::string simbol);
#endif
