#include "Gramatica.h"

#include <utility>
#include <queue>
#include <algorithm>
#include <iostream>

using std::vector;
using std::string;
using std::pair;
using std::queue;
using std::cout;

bool conditieBeta(string stanga, vector<string> dreapta)
{
    if (terminal(dreapta[0]))
    {
        return true;
    }
    else if (dreapta[0] > stanga)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool conditieAlpha(string stanga, vector<string> dreapta)
{
    if (dreapta[0] == stanga)
    {
        return true;
    }
    else
    {
        return false;
    }
}
vector<string> copiereVector(vector<string>& v,
                             int start, 
                             int lungime)
{
    vector<string> output(lungime);
    std::copy(v.begin() + start, 
              v.begin() + start + lungime, 
              output.begin());
    return output;
}
bool conditieDescompunere(Productie& p)
{
    //o productie poate fi descompusa
    //doar daca simbolurile din partea
    //dreapta satisfac conditieAlpha
    //sau conditieBeta
    MatriceString_t simboluriConditieAlpha =
        p.filtreazaDreapta(conditieAlpha);
    MatriceString_t simboluriConditieBeta =
        p.filtreazaDreapta(conditieBeta);

    const unsigned int sumaSimboluri = simboluriConditieAlpha.size()
                              +
                              simboluriConditieBeta.size();

    if (sumaSimboluri == p.dreapta().size())
    {
        return true;
    }
    else
    {
        return false;
    }
}

pair<Productie, Productie> descompuneProductie(Productie& p)
{
    Productie p1Rez;
    Productie p2Rez;
    queue<vector<string> > coada;
    const string simbolNou = p.stanga() + "'";
    //obtinem prima productie,
    //ce va cuprinde simbolurile
    //nerecursive la stanga
    p1Rez.stanga(p.stanga());
    MatriceString_t beta = p.filtreazaDreapta(conditieBeta);
    MatriceString_t::iterator betaIt;
    for (betaIt = beta.begin();
         betaIt != beta.end();
         ++betaIt)
    {
       p1Rez.adaugaDreapta(*betaIt);
       //adaugam la sfarsit noul simbol
       (*betaIt).push_back(simbolNou);
       coada.push((*betaIt));
    }

    while (coada.size() != 0)
    {
        vector<string> vs = coada.front();
        coada.pop();
        p1Rez.adaugaDreapta(vs);
    }
    //obtinem a doua productie,
    //ce va cuprinde noul simbol si
    //va elimina recursivitatea stanga
    MatriceString_t alpha = p.filtreazaDreapta(conditieAlpha);
    p2Rez.stanga(simbolNou);
    MatriceString_t::iterator alphaIt;
    for (alphaIt = alpha.begin();
         alphaIt != alpha.end();
         ++alphaIt)
    {
        //adaugam in noua regula toate simbolurile
        //in afara de primul
        vector<string> vs = 
            copiereVector(*alphaIt, 1, (*alphaIt).size() - 1);
        p2Rez.adaugaDreapta(vs);
        //adaugam la sfarsit noul simbol
        vs.push_back(simbolNou);
        //adaugam in coada
        coada.push(vs);
    }
    
    while (coada.size() != 0)
    {
        vector<string> vs = coada.front();
        coada.pop();
        p2Rez.adaugaDreapta(vs);
    }
    return std::make_pair(p1Rez, p2Rez);
}
Gramatica eliminaRecursivitateStanga(Gramatica& g)
{
    //extrage date din gramatica
    vector<string> N = g.N();
    vector<Productie> vectorProductii = g.P().productii();
    //create date pentru gramatica de iesire
    vector<string> Nprim = N;
    Productii Pprim;

    unsigned int pIdx = 0;
    for (pIdx = 0; pIdx < vectorProductii.size(); ++pIdx)
    {
        if (conditieDescompunere(vectorProductii[pIdx]))
        {
            pair<Productie, Productie> p = 
                descompuneProductie(vectorProductii[pIdx]);
            
            if (p.first.dreapta().size() > 0)
            {
                Pprim.adaugaProductie(p.first);
            }
            if (p.second.dreapta().size() > 0)
            {
                Pprim.adaugaProductie(p.second);
                Nprim.push_back(p.second.stanga());
            }
        }
    }

    return Gramatica(Nprim, g.sigma(), Pprim, g.S());
}
int main()
{
    
    vector<string> N;
    N.push_back("A"); N.push_back("B"); N.push_back("C");

    vector<string> sigma;
    sigma.push_back("a"); sigma.push_back("b");

    Productii P;
    Productie p1;
    p1.stanga("A");
    vector<string> p1Dreapta;
    p1Dreapta.push_back("A"); p1Dreapta.push_back("b");
    p1.adaugaDreapta(p1Dreapta);
    P.adaugaProductie(p1);

    vector<string> S;
    S.push_back("A");

    Gramatica g(N, sigma, P, S);
    Gramatica g1 = eliminaRecursivitateStanga(g);

    g.afisare();
    cout <<"\n\n";
    g1.afisare();
    /*
    Productie p;
    cin >> p;
    */
    return 0;
}
