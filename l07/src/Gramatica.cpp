#include "Gramatica.h"
#include <iostream>

using std::vector;
using std::string;
using std::cout;

void Productii::adaugaProductie(const Productie& p)
{
    productii_.push_back(p);
}

bool terminal(std::string simbol)
{
    unsigned int charIdx;
    for (charIdx = 0; charIdx < simbol.size(); ++charIdx)
    {
        if (simbol[charIdx] < 'a' || simbol[charIdx] > 'z')
        {
            return false;
        }
    }

    return true;
}

std::istream& operator >> (std::istream& is, Productie& p)
{
    //citire simbol stanga
    is >> p.stanga_;
    //citire simbol dreapta
    unsigned int nrSimboluriDreapta;
    is >> nrSimboluriDreapta;
    unsigned int simbolIdx;
    for (simbolIdx =  0; simbolIdx < nrSimboluriDreapta; ++simbolIdx)
    {
        //citim cate un grup de simboluri intr-un string
        //De exemplu: A->aC vom avea
        //grupSimboluri = "aC"
        std::string grupSimboluri;
        is >> grupSimboluri;
        //fiecare litera din grup va fi transformata intr-un
        //string si adaugata la vectorul final
        std::vector<std::string> vectorSimboluri;
        for (unsigned int simbolGrupIdx = 0; 
             simbolGrupIdx < grupSimboluri.size(); 
             ++simbolGrupIdx)
        {
            string simbol = string(grupSimboluri[simbolGrupIdx], 1);
            vectorSimboluri.push_back(simbol);
        }
        p.dreapta_.push_back(vectorSimboluri);
    }

    return is;
}
bool afisareProductie(string stanga, vector<string> dreapta)
{
    //functie care afiseaza fieacare productie
    //intoarce false deoarece e folosita in
    //metoda filtreazaDreapta a clasei productie
    //si metoda necesita o valoare de retur
    cout << stanga;
    cout << " -> ";
    unsigned int dreaptaIdx;
    for (dreaptaIdx = 0; dreaptaIdx < dreapta.size(); ++dreaptaIdx)
    {
        cout << dreapta[dreaptaIdx];
    }
    cout << "\n";
    return false;
}
void Gramatica::afisare()
{
    //afisare N
    cout << "N = {";
    unsigned int simbolIdx;
    for (simbolIdx = 0; simbolIdx < N_.size(); ++simbolIdx)
    {
        cout << N_[simbolIdx] << ", ";
    }
    cout << "}\n";

    //afisare sigma
    cout << "sigma = {";
    for (simbolIdx = 0; simbolIdx < sigma_.size(); ++simbolIdx)
    {
        cout << sigma_[simbolIdx] << ", ";
    }
    cout << "}\n";

    //afisare productii
    cout << "P\n";
    vector<Productie> productii = P_.productii();
    unsigned int productieIdx;
    for (productieIdx = 0; 
         productieIdx < productii.size();
         ++productieIdx)
    {
        productii[productieIdx].filtreazaDreapta(afisareProductie);
    }
    cout << "\n";

    //afisare S
    cout << "S = {";
    for (simbolIdx = 0; simbolIdx < S_.size(); ++simbolIdx)
    {
        cout << S_[simbolIdx] << ", ";
    }
    cout <<"}\n";
}
