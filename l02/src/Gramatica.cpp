#include "Gramatica.h"

#include <algorithm>
#include <iostream>

using std::vector;
using std::string;
using std::cout;

void Gramatica::afisare()
{
    //afisare N
    cout << "N = {";
    for (unsigned int nIdx = 0;
         nIdx < N_.size();
         ++nIdx)
    {
        cout << N_[nIdx] << ", ";
    }
    cout << "}\n";
    //afisare sigma
    cout << "sigma = {";
    for (unsigned int sigmaIdx = 0;
         sigmaIdx < sigma_.size();
         ++sigmaIdx)
    {
        cout << sigma_[sigmaIdx] << ", ";
    }
    cout << "}\n";
    //afisare simbol start
    cout << "S = " << S_ << "\n";
    //afisare reguli de productie
    cout << "P:\n";
    for (unsigned int pIdx = 0;
         pIdx < P_.size();
         ++pIdx)
    {
        cout << P_[pIdx] << "\n";
    }
}
void Gramatica::transformaDinAutomat(Automat& a)
{
    vector<int> q = a.q();
    const unsigned int sizeQ = q.size();
    //pentru fiecare stare din a se asociaza
    //litera mare corespunzatoare: 0 -> A, 1 -> B
    N_.resize(sizeQ);
    for (unsigned int i = 0;
         i < sizeQ;
         ++i)
    {
        N_[i] = 'A' + q[i];
    }

    //alfabetul va fi acelasi ca al automatului
    sigma_ = a.sigma();//se apeleaza constructorul de copiere

    //simbolul de start este litera corespunzatoare
    //starii de start a automatului
    S_ = 'A' + a.q0();

    vector<int> f = a.f();
    
    //formare vector cu reguli de productie
    vector<Tranzitie> tranzitii = a.delta().tranzitii();
    vector<Tranzitie>::iterator itTranzitie;
    for (itTranzitie = tranzitii.begin();
         itTranzitie != tranzitii.end();
         ++itTranzitie)
    {
        const int s1 = (*itTranzitie).stareInitiala;
        const char simbol = (*itTranzitie).simbol;
        const int s2 = (*itTranzitie).stareFinala;
        
        //daca s2 se gaseste in multimea starilor finale,
        //atunci regula va fi s1 -> simbol
        vector<int>::iterator it =
            std::find(f.begin(), f.end(), s2);
        if (it != f.end())
        {
            string regulaProductie;
            regulaProductie += static_cast<char>(s1 + 'A');
            regulaProductie += " -> ";
            regulaProductie += simbol;
            P_.push_back(regulaProductie);
        }
        else
        {
            //altfel, regula va fi s1 -> simbolS2
            string regulaProductie;
            regulaProductie += static_cast<char>(s1 + 'A');
            regulaProductie += " -> ";
            regulaProductie += simbol;
            regulaProductie +=  static_cast<char>(s2 + 'A');
            P_.push_back(regulaProductie);
        }
    }
}
