#include "Automat.h"
#include "Gramatica.h"

#include <iostream>
#include <string>
#include <map>
#include <set>

using std::string;
using std::vector;
using std::map;
using std::set;
using std::pair;

map<int, string> mesajeStariProblema1a()
{
    map<int, string> m;
    m[0] = "mal stang: om, capra, varza, lup\tmal drept: \n";
    m[1] = "mal stang: varza, lup\t mal drept: om, capra\n";
    m[2] = "mal stang: om, varza, lup\t mal drept: capra\n";
    m[3] = "mal stang: lup\t mal drept: om, capra, varza\n";
    m[4] = "mal stang: varza\t mal drept: om, capra, lup\n";
    m[5] = "mal stang: om, capra, lup\t mal drept: varza\n";
    m[6] = "mal stang: om, capra, varza\t mal drept: lup\n";
    m[7] = "mal stang: capra\t mal drept: om, varza, lup\n";
    m[8] = "mal stang: om, capra\t mal drept: varza, lup\n";
    m[9] = "mal stang: \t mal drept: om, capra, varza, lup\n";
    m[10] = "eroare\n";
    m[-1] = "stare invalida\n";

    return m;
}

map<int, string> mesajeStariProblema1b()
{
    //INSPIRATION:
    //http://www.aiai.ed.ac.uk/~gwickler/missionaries.html
    map<int, string> m;
    m[0] = "mal stang: 3m, 3c, b\tmal drept: \n";
    
    m[1] = "mal stang: 3m, 1c\tmal drept: 2c, b\n";
    m[2] = "mal stang: 2m, 2c\tmal drept: 1m, 1c, b\n";
    m[3] = "mal stang: 3m, 2c\tmal drept: 1c, b\n";

    m[4] = "mal stang: 3m, 2c, b\tmal drept: 1c\n";
    m[5] = "mal stang: 3m\tmal drept: 3c, b\n";
    m[6] = "mal stang: 3m, 1c, b\tmal drept: 2c\n";
    m[7] = "mal stang: 1m, 1c\tmal drept: 2m, 2c, b\n";

    m[8] = "mal stang: 2m, 2c, b\tmal drept: 1m, 1c\n";
    m[9] = "mal stang: 2c\tmal drept: 3m, 1c, b\n";
    m[10] = "mal stang: 3c, b\tmal drept: 3m\n";
    m[11] = "mal stang: 1c\tmal drept: 3m, 2c, b\n";

    m[12] = "mal stang: 2c, b\tmal drept: 3m, 1c\n";
    m[13] = "mal stang: 1m, 1c, b\tmal drept: 2m, 2c\n";
    m[14] = "mal stang: 1c, b\tmal drept: 3m, 2c\n";

    m[15] = "mal stang: \tmal drept: 3m, 3c, b\n";
    m[-1] = "stare invalida\n";

    return m;
}

map<char, string> mesajeTranzitiiProblema1a()
{
    map<char, string> m;
    m['o'] = "omul trece pe celalalt mal\n";
    m['c'] = "omul trece cu capra pe celalalt mal\n";
    m['v'] = "omul trece cu varza pe celalalt mal\n";
    m['l'] = "omul trece cu lupul pe celalalt mal\n";

    return m;
}

map<char, string> mesajeTranzitiiProblema1b()
{
    map<char, string> m;
    m['m'] = "un misionar trece cu barca pe celalalt mal\n";
    m['M'] = "doi misionari trec cu barca pe celalalt mal\n";
    m['c'] = "un canibal trece cu barca pe celalalt mal\n";
    m['C'] = "doi canibali trec cu barca pe celalalt mal\n";
    m['x'] = "un misionar si un canibal trec cu barca pe celalalt mal\n";
    
    return m;
}
int main()
{
    //problema1a
    map<int, string> msa = mesajeStariProblema1a();
    map<char, string> mta = mesajeTranzitiiProblema1a();
    Automat a;
    a.citesteDinFisier("problema1a.txt");
    a.afisare();
    string cuvant;
    std::cout << "Introduceti cuvant: ";
    std::cin >> cuvant;
    bool cuvantEsteAcceptat = limbajRecunoscut(cuvant, a, msa, mta);
    std::cout << "Cuvantul este acceptat: "
              << cuvantEsteAcceptat << "\n";
    
    //problema1b
    map<int, string> msb = mesajeStariProblema1b();
    map<char, string> mtb = mesajeTranzitiiProblema1b();
    Automat a2;
    a2.citesteDinFisier("problema1b.txt");
    a2.afisare();
    std::cout << "Introduceti cuvant: ";
    std::cin >> cuvant;
    cuvantEsteAcceptat = limbajRecunoscut(cuvant, a2, msb, mtb);
    std::cout << "Cuvantul este acceptat: "
              << cuvantEsteAcceptat << "\n";
    
    //problema2
    Automat a3;
    a3.citesteDinFisier("input.txt");
    a3.afisare();
    Gramatica g;
    g.transformaDinAutomat(a3);
    std::cout << "Gramatica formata pe baza automatului este:\n";
    g.afisare();
    return 0;
}
