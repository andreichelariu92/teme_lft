#ifndef Automat_H_INCLUDE_GUARD
#define Automat_H_INCLUDE_GUARD

#include <vector>
#include <string>
#include <utility>
#include <map>

typedef std::map<int, std::string> MesajeStari;
typedef std::map<char, std::string> MesajeTranzitii;

///structura ce defineste o
///tranzitie din starea initiala
///in starea finala folosind
///simbolul "simbol"
struct Tranzitie
{
    int stareInitiala;
    char simbol;
    int stareFinala;

    Tranzitie(int sI, char s, int sF)
        :stareInitiala(sI),
         simbol(s),
         stareFinala(sF)
    {}

    Tranzitie()
        :stareInitiala(0),
         simbol(0),
         stareFinala(0)
    {}
};

///clasa care defineste matricea de
///tranzitie a unui automat finit determinist
///ofera in plus metode ajutatoare
class FunctieDeTranzitie
{
    private:
        std::vector<Tranzitie> tranzitii_;
    public:
        FunctieDeTranzitie();
        FunctieDeTranzitie(std::vector<Tranzitie> tranzitii);
        //efectueaza o tranzitie din starea "stare" cu
        //ajutorul simbolului simbol
        //intoarce noua stare dupa tranzitie sau -1
        //daca nu se poate face tranzitia
        int efectueazaTranzitie(int stare,
                                char simbol,
                                std::string mesaj = "");
        ///intoarce un vector cu posibilele tranzitii
        ///din starea "stare"
        ///daca nu exista tranzitii,
        ///vectorul returnat va avea dimensiune 0
        std::vector<std::pair<char, int> > gasesteTranzitii(int stare);
        ///metoda de acces la tranizii
        std::vector<Tranzitie>& tranzitii()
        {
            return tranzitii_;
        }
};

///clasa ce defineste un automat
///finit determinist
class Automat
{
private:
    //multimea starilor
    std::vector<int> q_;
    //multimea simbolurilor
    std::vector<char> sigma_;
    //starea initiala
    int q0_;
    //multimea starilor finale
    std::vector<int> f_;
    //functie de tranzitie,
    //definita prin vector de tranzitii
    FunctieDeTranzitie delta_;
public:
    ///constructorul, operatorul= si
    ///destructorul sunt generati implicit
    ///de compilator
    
    ///se presupune ca fisierul e scris corect :)
    ///daca nu, atunci comportamentul functiei
    ///e nedefinit
    void citesteDinFisier(std::string fileName);

    ///metode de acces la variabilele automatului
    std::vector<int>& q()
    {
        return q_;
    }
    std::vector<char>& sigma()
    {
        return sigma_;
    }
    int q0()
    {
        return q0_;
    }
    std::vector<int>& f()
    {
        return f_;
    }
    FunctieDeTranzitie& delta()
    {
        return delta_;
    }
    void afisare();

};

//intoarce true daca prin parcurgerea simbolurilor
//din cuvant ajungem intr-o stare finala
bool limbajRecunoscut(std::string cuvant,
                      Automat& automat,
                      MesajeStari mesajeStari = MesajeStari(),
                      MesajeTranzitii mesajeTranzitii = MesajeTranzitii());

//calculeaza cel mai scurt drum de la stareaInitiala la cea finala
//daca nu exista drum, atunci va returna litera '255'
//daca starile coincid, atunci va returna cuvantul vid ""
std::string Dijkstra(Automat& automat, int stareInitiala, int stareFinala);

//obiect cu ajutorul caruia
//vom tine starile in ordine
//pe baza drumului pana la ele,
//in ordine crescatoare
struct Comparator
{
    //referinta la map-ul din functia de
    //mai jos
    //folosim referinta, pentru a primi
    //modificarile facute pe parcursul
    //algoritmului
    std::map<int, std::string>& distanta;
    //constructor
    Comparator(std::map<int, std::string>& d)
        :distanta(d)
    {}
    //functie care compara doua stari pe
    //baza distantei
    //intoarce true daca stare1 are distanta
    //mai mica si false in caz contrar
    bool operator()(int stare1, int stare2)
    {
        //returnez valoarea intoarsa de operatorul <
        return (distanta[stare1] < distanta[stare2]);
    }
};

#endif
