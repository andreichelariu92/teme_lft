#include "Automat.h"

#include <fstream>
#include <iostream>
#include <set>

using std::vector;
using std::pair;
using std::cout;
using std::string;
using std::set;
using std::map;

FunctieDeTranzitie::FunctieDeTranzitie()
    :tranzitii_()
{}

FunctieDeTranzitie::FunctieDeTranzitie
                    (vector<Tranzitie> tranzitii)
    :tranzitii_(tranzitii)
{}

int FunctieDeTranzitie::efectueazaTranzitie
                        (int stare, char simbol, string mesaj)
{
    vector<Tranzitie>::iterator it;
    //stareRezultat este -1, marcata
    //ca nevalida, daca se va gasi o stare
    //in vector pentru care se va putea face
    //tranzitia, valoarea lui stareRezultat
    //va fi actualizata
    int stareRezultat = -1;
    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare &&
            (*it).simbol == simbol)
        {
            stareRezultat = (*it).stareFinala;
        }
    }
    
    //se afiseaza mesajul doar daca trazitia
    //e valida si mesajul contine ceva
    if (mesaj != "")
    {
        std::cout << mesaj;
        std::cout << "~~~~~~~~~~~~~~~~~~~~\n";
    }

    return stareRezultat;
}

vector<pair<char, int> > FunctieDeTranzitie::gasesteTranzitii
                         (int stare)
{
    vector<pair<char, int> > tranzitiiPosibile;
    vector<Tranzitie>::iterator it;

    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare)
        {
            pair<char, int> p((*it).simbol, (*it).stareFinala);
            tranzitiiPosibile.push_back(p);
        }
    }

    return tranzitiiPosibile;
}
void Automat::citesteDinFisier(std::string fileName)
{
    std::ifstream f(fileName.c_str(), std::ifstream::in);

    //citire stari
    int nrStari = 0;
    f >> nrStari;
    q_.resize(nrStari);
    for (int stareIdx = 0; stareIdx < nrStari; ++stareIdx)
        f >> q_[stareIdx];
    //citire simboluri
    int nrSimboluri = 0;
    f >> nrSimboluri;
    sigma_.resize(nrSimboluri);
    for (unsigned int simbolIdx = 0; 
         simbolIdx < nrSimboluri; 
         ++simbolIdx)
    {
        f >> sigma_[simbolIdx];
    }
    //citire stare initiala
    f >> q0_;
    //citire stari finale
    int nrStariFinale = 0;
    f >> nrStariFinale;
    f_.resize(nrStariFinale);
    for (unsigned int stareFIdx = 0; 
         stareFIdx < nrStariFinale; 
         ++stareFIdx)
    {
        f >> f_[stareFIdx];
    }
    //citire functie tranzitie
    int stareInitiala;
    char simbol;
    int stareFinala;
    vector<Tranzitie> tranzitii;
    while (f >> stareInitiala >> simbol >> stareFinala)
    {
        Tranzitie t(stareInitiala, simbol, stareFinala);
        tranzitii.push_back(t);
    }
    delta_ = FunctieDeTranzitie(tranzitii);
}
void Automat::afisare()
{
    cout << "Q = {";
    for (unsigned int i = 0; i < q_.size(); ++i)
        std::cout << q_[i] << ", ";
    std::cout << "}\n";
    
    std::cout << "Sigma = {";
    for (unsigned int i = 0; i < sigma_.size(); ++i)
    {
        std::cout << sigma_[i] << ", ";
    }
    std::cout << "}\n";
    
    std::cout << "Q0 = ";
    std::cout << q0_ << "\n";
    
    std::cout << "F = {";
    for (unsigned int i = 0; i < f_.size(); ++i)
    {
        std::cout << f_[i] << ", ";
    }
    std::cout << "}\n";
    
    std::cout << "Delta(functia de tranzitie):\n";
    std::vector<Tranzitie>& tranzitii = delta_.tranzitii();
    for (unsigned int i = 0; i < tranzitii.size(); ++i)
    {
        std::cout << tranzitii[i].stareInitiala << " ";
        std::cout << tranzitii[i].simbol << " ";
        std::cout << tranzitii[i].stareFinala << "\n";
    }
    std::cout << "\n";
}

bool limbajRecunoscut(std::string cuvant,
                      Automat& automat,
                      MesajeStari mesajeStari,
                      MesajeTranzitii mesajeTranzitii)
{
    FunctieDeTranzitie& f = automat.delta();
    const unsigned int dimCuvant = cuvant.size();
    int stare= automat.q0();

    for (unsigned int charIdx = 0; charIdx < dimCuvant; ++charIdx)
    {
       //compune mesaj de afisare
       string mesajAfisare;
       if (mesajeStari.find(stare) != mesajeStari.end())
       {
           mesajAfisare += mesajeStari[stare];
       }
       if (mesajeTranzitii.find(cuvant[charIdx]) != mesajeTranzitii.end())
       {
           mesajAfisare += mesajeTranzitii[cuvant[charIdx]];
       }
       //se efectueaza cate o tranzitie pentru fiecare
       //simbol(litera) a cuvantului
       //daca se ajunge intr-o stare invalida(nu se poate
       //face tranzitie), atunci se returneaza false 
       stare = f.efectueazaTranzitie(stare, cuvant[charIdx], mesajAfisare); 
       if (stare == -1)
       {
           //afiseaza starea finala inainte
           //de a parasi functia
           if (mesajeStari.find(stare) != mesajeStari.end())
           {
               cout << mesajeStari[stare];
           }
           return false;
       }
    }

    //se verifica daca starea in care am ajuns dupa efectuarea
    //tututor tranzitiilor face parte din multimea starilor
    //finale
    vector<int>& stariFinale = automat.f();
    const unsigned int dimStariFinale = stariFinale.size();

    for (unsigned int stariFinaleIdx = 0; 
         stariFinaleIdx < dimStariFinale;
         ++stariFinaleIdx)
    {
        if (stare == stariFinale[stariFinaleIdx])
        {
           if (mesajeStari.find(stare) != mesajeStari.end())
           {
               cout << mesajeStari[stare];
           }
           return true;
        }
    }

    //daca starea finala in care am ajuns nu se gaseste
    //in multimea starilor finale, se returneaza false
    if (mesajeStari.find(stare) != mesajeStari.end())
    {
        cout << mesajeStari[stare];
    }
    return false;
}

string infinit()
{
    string s;
    s += static_cast<char>(255);
    return s;
}

bool conditieDeStop(map<int, string>& distanta,
                    set<int, Comparator>& stariNevizitate)
{
    //daca setul e vid se intoarce true
    if (stariNevizitate.begin() ==
        stariNevizitate.end())
    {
        return true;
    }

    //se verifica daca prima stare din cele nevizitate
    //are distanta infinit
    const int primaStareNevizitata = *(stariNevizitate.begin());
    if (distanta[primaStareNevizitata] == 
        infinit())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//se actualizeaza starile nevizitate
//prin stergerea si adaugarea starii modificate
//in acest fel se reorganizeaza setul
void actualizeazaStariNevizitate
     (set<int, Comparator>& stariNevizitate,
      int stareModificata)
{
    stariNevizitate.erase(stareModificata);
    stariNevizitate.insert(stareModificata);
}

//calculeaza cel mai scurt drum de la stareaInitiala la cea finala
//daca nu exista drum, atunci va returna litera '255'
//daca starile coincid, atunci va returna cuvantul vid ""
string Dijkstra(Automat& automat, int stareInitiala, int stareFinala)
{
    //INSPIRATIE: 
    //https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm 
    
    //map ce asociaza fiecarei stari din automat o distanta
    map<int, string> distanta;
    //set care memoreaza starile nevizitate in ordine crescatoare,
    //pe baza distantei pana la ele
    Comparator comparator(distanta);
    set<int, Comparator> stariNevizitate(comparator);

    //initializare structuri de date
    distanta[stareInitiala] = "";
    stariNevizitate.insert(stareInitiala);
    std::vector<int>& stari = automat.q();
    for (unsigned int stareIdx = 0; 
         stareIdx < stari.size(); 
         ++stareIdx)
    {
        if (stari[stareIdx] != stareInitiala)
        {
            //asociem fiecarei stari valoarea infinit pentru
            //distanta
            distanta[ stari[stareIdx] ] = infinit();
            //adaugam toate starile, in afara de cea initiala
            //in stariNevizitate
            stariNevizitate.insert(stari[stareIdx]);
        }
    }

    int stareCurenta = stareInitiala;
    bool exit = false;
    while (!exit)
    {
        //luam toate starile in care putem
        //ajunge din starea curenta
        vector<pair<char, int> > tranzitii =
            automat.delta().gasesteTranzitii(stareCurenta);
        //parcurg toate tranzitiile vecine
        vector<pair<char, int> >::iterator itTranzitie;
        for (itTranzitie = tranzitii.begin();
             itTranzitie != tranzitii.end();
             ++itTranzitie)
        {
            //calcul distanta tentativa
            string distantaTentativa = distanta[stareCurenta];
            distantaTentativa += (*itTranzitie).first;
            //daca distanta tentativa este mai mica decat
            //cea din map se actualizeaza distanta
            if (distanta[(*itTranzitie).second] >
                distantaTentativa)
            {
                distanta[(*itTranzitie).second] =
                    distantaTentativa;

                actualizeazaStariNevizitate
                (stariNevizitate,
                 (*itTranzitie).second);
            }
        }
        //scot starea curenta din stariNevizitate
        stariNevizitate.erase(stareCurenta);
        exit = conditieDeStop(distanta, stariNevizitate);
        //se alege ca stare curenta prima stare din set
        if (!exit)
        {
            stareCurenta = *(stariNevizitate.begin());
        }
    }

    return distanta[stareFinala];
}
