#include <iostream>
#include <algorithm>

#include "Automat.h"

using std::string;
using std::vector;
using std::cout;

typedef vector<vector<int> > matrice;

bool cautaVector(vector<int>& v, int val)
{
    vector<int>::iterator pozitie;
    pozitie = std::find(v.begin(), v.end(), val);
    if (pozitie != v.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}
vector<int> diferentaVectori(vector<int>& a, vector<int>& b)
{
    vector<int> output;
    for (unsigned int aIdx = 0; aIdx < a.size(); ++aIdx)
    {
        if (cautaVector(b, a[aIdx]) == false)
        {
            output.push_back(a[aIdx]);
        }
    }

    return output;
}
vector<int> eliminareStari(Automat& automat)
{
    vector<int> output;
    vector<int> stariAccesibile = automat.stariAccesibile();
    vector<int> stariFinalizate = automat.stariFinalizate();
    vector<int>& automatQ = automat.q();
    //adaug in vectorul de iesire doar
    //starile care fac parte din cele
    //accesibile si finalizate
    for (unsigned int qIdx = 0;
         qIdx < automatQ.size();
         ++qIdx)
    {
        if (cautaVector(stariAccesibile, automatQ[qIdx]) &&
            cautaVector(stariFinalizate, automatQ[qIdx]))
        {
            output.push_back(automatQ[qIdx]);
        }
    }

    return output;
}
Automat minimizareAutomat(Automat& automat)
{
    //extragem datele din automat
    vector<int>& f = automat.f();
    vector<int> q = automat.q();
    vector<int> qMinusF = diferentaVectori(q, f);
    vector<char> sigma = automat.sigma();
    FunctieDeTranzitie delta = automat.delta();
    int q0 = automat.q0();

    //initializare matrice
    matrice A(automat.q().size());
    for (unsigned int i = 0; i < A.size(); ++i)
    {
        //presupunem la inceput ca toate elementele
        //sunt indistinctibile intre ele
        A[i] = vector<int>(automat.q().size());
    }

    //marcam ca distinctibile elementele care fac parte
    //din clase diferite de stari(nefinale si finale)
    for (unsigned int i = 0; i < q.size() - 1; ++i)
    {
        for (unsigned int j = i+1; j < q.size(); ++j)
        {
            if ((cautaVector(f, q[i]) && cautaVector(qMinusF, q[j]))
                ||
                (cautaVector(qMinusF, q[i]) && cautaVector(f, q[j])))
            {
                A[i][j] = 1;
                A[j][i] = 1;
            }
        }
    }
    
    bool modif = false;
    do
    {
        modif = false;
        for (unsigned int i = 0; i < q.size(); ++i)
        {
            for (unsigned int j = 0; j < q.size(); ++j)
            {
                if (A[i][j] == 0)
                {
                    for (unsigned int sigmaIdx = 0;
                         sigmaIdx < sigma.size();
                         ++sigmaIdx)
                    {
                        int tranzitieQI = delta.efectueazaTranzitie(q[i], sigma[sigmaIdx]);
                        int tranzitieQJ = delta.efectueazaTranzitie(q[j], sigma[sigmaIdx]);
                        //daca doar una din tranzitii nu este valida
                        //,atunci starile sunt distinctibile
                        if ((tranzitieQI < 0 && tranzitieQJ > 0)
                            ||
                            (tranzitieQI > 0 && tranzitieQJ < 0))
                        {
                            A[i][j] = 1;
                            A[j][i] = 1;
                            modif = true;
                            //daca am gasit un simbol pentru
                            //care ne dam seama ca starile
                            //sunt distinctibile nu mai
                            //cautam si pentru restul
                            //simbolurilor
                            break;
                        }
                        else
                        {
                            if (tranzitieQI > 0 && tranzitieQJ > 0)
                            {
                                //daca noile stari in care am
                                //ajuns sunt distinctibile,
                                //atunci si starile initiale
                                //sunt distinctibile
                                if (A[tranzitieQI][tranzitieQJ]
                                    != 0)
                                {
                                    A[i][j] = 1;
                                    A[j][i] = 1;
                                    modif = true;
                                    //la fel ca mai sus :)
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

    }while(modif);
    
    //vectorul clasa contine pentru fiecare
    //stare initiala ce stare noua ii va corespunde
    vector<int> clasa(q.size(), -1);
    int nrClase = -1;
    //tine pentru fiecare noua stare din
    //noul automat ce stare din automatul initial
    //ii corespunde
    vector<int> reprezentantClasa;
    //multimea starilor finale din noul
    //automat
    vector<int> fPrim;
    for (unsigned int i = 0; i < q.size(); ++i)
    {
        if (clasa[i] == -1)
        {
            //in nrClase se tine noua stare
            //din noul automat
            clasa[i] = ++nrClase;
            reprezentantClasa.push_back(i);
            if (cautaVector(f, q[i]) == true)
            {
                fPrim.push_back(nrClase);
            }
            //toate starile indistinctibile
            //cu starea q[i] vor face parte
            //din aceeasi clasa
            for (unsigned int j = i + 1; j < q.size(); ++j)
            {
                if (A[i][j] == 0)
                {
                    clasa[j] = nrClase;
                }
            }
        }
    }

    //contruire automat echivalent
    vector<int> qPrim(nrClase + 1);
    vector<Tranzitie> deltaPrim;
    int q0Prim = clasa[q0];

    for (unsigned int i = 0; i < qPrim.size(); ++i)
    {
        qPrim[i] = i;
    }
    for (unsigned int i = 0; i < qPrim.size(); ++i)
    {
        for (unsigned int sigmaIdx = 0; sigmaIdx < sigma.size();
             ++sigmaIdx)
        {
            int nouaStare = delta.efectueazaTranzitie
                (reprezentantClasa[i], sigma[sigmaIdx]);
            if (nouaStare != -1)
            {
                Tranzitie t(i, sigma[sigmaIdx], clasa[nouaStare]);
                deltaPrim.push_back(t);
            }
        }
    }

    Automat automatMinimizat(qPrim, sigma, 
            q0Prim, fPrim, deltaPrim);
    
    return automatMinimizat;
}
int main()
{
    Automat a;
    a.citesteDinFisier("./input.txt");
    //a.afisare();
    cout << "\n";

    Automat minim = minimizareAutomat(a);
    minim.afisare();

    return 0;
}
