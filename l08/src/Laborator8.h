#ifndef Laborator8_H_INCLUDE_GUARD
#define Laborator8_H_INCLUDE_GUARD

#include "Gramatica.h"

#include <string>
#include <vector>
#include <algorithm>

struct ConditieTerminal
{
    std::string terminal;
    ConditieTerminal(std::string argTerminal)
        :terminal(argTerminal)
    {}
    bool operator()(std::string stanga,
                    std::vector<std::string> dreapta);
};

typedef std::vector<std::vector<std::vector<std::string> > > CubString_t;
struct ConditieMatrice
{
	CubString_t& V;
	unsigned int i;
	unsigned int j;
	unsigned int k;

	ConditieMatrice(CubString_t& argV,
			unsigned int argI,
			unsigned int argJ,
			unsigned int argK)
		:V(argV),
		i(argI),
		j(argJ),
		k(argK)
	{}

	bool operator() (std::string stanga, 
                     std::vector<std::string> dreapta);

};

CubString_t cuvantAcceptat(Gramatica& g, std::string cuvant);

void afisareV(CubString_t& V);


bool cautaVector(std::vector<std::string>& v, std::string val);
#endif
