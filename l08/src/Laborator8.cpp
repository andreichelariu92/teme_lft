#include "Laborator8.h"

#include <iostream>
#include <iomanip>

using std::string;
using std::vector;
using std::cout;

CubString_t cuvantAcceptat(Gramatica& g, string cuvant)
{
	//deoarece toate for-urile pornesc
	//de la 1, adaugam un caracter fictiv
	//la sirul de caractere pentru a nu
	//avea probleme cu indicii
	cuvant = " " + cuvant;
    const unsigned int n = cuvant.size() - 1;
	//initializare matrice
	vector<string> vectorGol;
	vector<vector<string> > matriceGoala(cuvant.size(), vectorGol);
	vector<vector<vector<string> > > V(cuvant.size(), matriceGoala);

	unsigned int i;
	//pentru fiecare litera din cuvant,
	//verificam daca exista o productie
	//care ne duce in acel simbol
	for (i = 1; i <= n; ++i)
	{

		ConditieTerminal ct(string(1, cuvant[i]));
		vector<Productie> productiiTerminale = g.P().filtreazaProductii(ct);

		//adaugam simbolul de start
		//pentru fiecare productie care
		//respecta conditia in matrice
		vector<Productie>::iterator productieIt;
		for (productieIt = productiiTerminale.begin();
		     productieIt != productiiTerminale.end();
		     ++productieIt)
		{
		    (V[i][1]).push_back((*productieIt).stanga());
		}

	}

	unsigned int j;
	for (j = 2; j <= n; ++j)
	{
		for (i = 1; i <= n - j + 1; ++i)
		{
			//pentru fiecare productie care
			//se duce in 2 simboluri, verificam
			//daca cele 2 simboluri sunt in matricea V
			unsigned int k;
			for (k = 1; k <= j -1; ++k)
			{
			    ConditieMatrice cm(V, i, j, k);
			    vector<Productie> productiiMatrice = g.P().filtreazaProductii(cm);

			    //adaugam simbolul de start
			    //pentru fiecare regula care
			    //respecta conditia in matrice
			    vector<Productie>::iterator productieIt;
			    for (productieIt = productiiMatrice.begin();
				 productieIt != productiiMatrice.end();
				 ++productieIt)
			     {
                    const string stanga = (*productieIt).stanga();
                    if (cautaVector(V[i][j], stanga) == false)
                    {
				        V[i][j].push_back(stanga);
                    }
			     }
			}
		}
	}

    return V;
}


bool ConditieTerminal::operator()(string stanga, 
                                  vector<string> dreapta)
{
    if (dreapta.size() == 1 && dreapta[0] == terminal)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ConditieMatrice::operator() (string stanga, vector<string> dreapta)
{
    if (dreapta.size() == 2)
    {
        vector<string>::iterator dreapta0It =
            std::find(V[i][k].begin(), V[i][k].end(), dreapta[0]);
        vector<string>::iterator dreapta1It =
            std::find(V[i + k][j - k].begin(), V[i + k][j - k].end(), dreapta[1]);
        if (dreapta0It != V[i][k].end()
            &&
            dreapta1It != V[i + k][j - k].end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void afisareV(CubString_t& V)
{
    unsigned int rowIdx;
    for(rowIdx = 1; rowIdx < V.size(); ++rowIdx)
    {
        unsigned int colIdx;
        for (colIdx = 1; colIdx < V[rowIdx].size(); ++colIdx)
        {
            cout << std::setw(V.size());
            if (V[rowIdx][colIdx].size() == 0)
            {
                cout << "V";
            }
            unsigned int simbolIdx;
            for (simbolIdx = 0;
                 simbolIdx < V[rowIdx][colIdx].size();
                 ++simbolIdx)
            {
                std::cout << (V[rowIdx][colIdx])[simbolIdx];
            }

            std::cout <<"|";
        }

        std::cout <<"\n";
    }
}

bool cautaVector(vector<string>& v, string val)
{
    vector<string>::iterator pozitieVal =
        std::find(v.begin(), v.end(), val);

    if (pozitieVal != v.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}
