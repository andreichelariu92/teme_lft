#include "Gramatica.h"
#include "Laborator8.h"

#include <iostream>

using std::cout;
using std::string;

int main()
{
    Gramatica g;
    g.citireDinFisier("input.txt");
    //g.afisare();
    
    string cuvant("baaba");
    CubString_t V = cuvantAcceptat(g, cuvant);
    afisareV(V);

    if (cautaVector(V[1][cuvant.size()], g.S()))
    {
        cout << "Cuvantul este acceptat\n";
    }
    else
    {
        cout << "Cuvantul nu este acceptat\n";
    }
    return 0;
}
