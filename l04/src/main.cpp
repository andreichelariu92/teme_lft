#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <stack>

#include "Automat.h"

using std::string;
using std::vector;
using std::cout;
using std::stack;

//pas1
bool apartineVector(vector<char>& v, char c)
{
    vector<char>::iterator it;
    it = std::find(v.begin(), v.end(), c);
    if (it != v.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}
string pas1(string w1, const vector<char>& sigma)
{
    //configurare X
    vector<char> X(sigma.size());
    std::copy(sigma.begin(), sigma.end(), X.begin());
    X.push_back('E');
    X.push_back('V');
    X.push_back(')');
    X.push_back('*');
    X.push_back('#');

    //configurare Y
    vector<char> Y(sigma.size());
    std::copy(sigma.begin(), sigma.end(), Y.begin());
    Y.push_back('E');
    Y.push_back('V');
    Y.push_back('(');

    char buffer[75];
    unsigned int i = 0;
    unsigned int j = 1;
    buffer[0] = w1[0];

    for (i = 1; i < w1.size(); ++i)
    {
        if (apartineVector(Y, w1[i]) &&
            apartineVector(X, w1[i-1]))
        {
            buffer[j++] = '.';
        }
        buffer[j++] = w1[i];
    }

    return string(buffer, j);
}

//pas2
int poe(char semn)
{
    switch (semn)
    {
        case '*':
        case '#':
            return 3;
        case '.':
            return 2;
        case '+':
            return 1;
        case '(':
            return 4;
        case '_':
            return 0;
        default:
            return -1;

    }
}

int pos(char semn)
{
    switch (semn)
    {
        case '*':
        case '#':
            return 3;
        case '.':
            return 2;
        case '+':
            return 1;
        case '(':
            return 0;
        case '_':
            return -1;
        default:
            return -2;
    }
}
string pas2(string w2, const vector<char>& sigma)
{
    vector<char> simboluri(sigma.size());
    std::copy(sigma.begin(), sigma.end(), simboluri.begin());
    simboluri.push_back('V');
    simboluri.push_back('E');

    stack<char> S;
    string w3;
    S.push('_');
    for (unsigned int i = 0; i < w2.size(); ++i)
    {
        if (apartineVector(simboluri, w2[i]))
        {
            w3 += w2[i];
        }
        else
        {
            if (w2[i] == ')')
            {
                while (S.top() != '(')
                {
                    w3 += S.top();
                    S.pop();
                }
                S.pop();
            }
            else
            {
                while (pos(S.top()) >= poe(w2[i]))
                {
                    w3 += S.top();
                    S.pop();
                }
                S.push(w2[i]);
            }
        }
    }

    while (S.top() != '_')
    {
        w3 += S.top();
        S.pop();
    }
    S.pop();

    return w3;
}
//pas3
struct SistemTranzitional
{
    int initial;
    int final;

    SistemTranzitional(int i, int f)
        :initial(i), final(f)
    {}

    SistemTranzitional()
        :initial(0), final(0)
    {}
};
Automat pas3(string w3, const vector<char>& sigma)
{
    vector<char> simboluri(sigma.size());
    std::copy(sigma.begin(), sigma.end(), simboluri.begin());
    simboluri.push_back('V');
    simboluri.push_back('E');

    int stCrt = -1;
    stack<SistemTranzitional> stivaST;
    vector<Tranzitie> delta;
    
    SistemTranzitional M2;
    SistemTranzitional M1;

    for (unsigned int i = 0; i < w3.size(); ++i)
    {
        if (apartineVector(simboluri, w3[i]))
        {
            ++stCrt;
            stivaST.push(SistemTranzitional(stCrt, stCrt + 1));

            if (w3[i] != 'V')
            {
                delta.push_back(Tranzitie(stCrt, w3[i], stCrt + 1));
                ++stCrt;
            }
        }
        else
        {
            
            switch (w3[i])
            {
                case '+':
                    M2 = stivaST.top();
                    stivaST.pop();
                    M1 = stivaST.top();
                    stivaST.pop();
                    ++stCrt;
                    delta.push_back(Tranzitie(stCrt, 'E', M1.initial));
                    delta.push_back(Tranzitie(stCrt, 'E', M2.initial));
                    delta.push_back(Tranzitie(M1.final, 'E', stCrt + 1));
                    delta.push_back(Tranzitie(M2.final, 'E', stCrt + 1));
                    stivaST.push(SistemTranzitional(stCrt, stCrt + 1));
                    ++stCrt;
                    break;
                case '.':
                    M2 = stivaST.top();
                    stivaST.pop();
                    M1 = stivaST.top();
                    stivaST.pop();
                    delta.push_back(Tranzitie(M1.final, 'E', M2.initial));
                    stivaST.push(SistemTranzitional(M1.initial, M2.final));
                    break;
                case '#':
                    M1 = stivaST.top();
                    stivaST.pop();
                    ++stCrt;
                    delta.push_back(Tranzitie(stCrt, 'E', M1.initial));
                    delta.push_back(Tranzitie(M1.final, 'E', M1.initial));
                    delta.push_back(Tranzitie(M1.final, 'E', stCrt + 1));
                    stivaST.push(SistemTranzitional(stCrt, stCrt + 1));
                    ++stCrt;
                    break;
                case '*':
                    M1 = stivaST.top();
                    stivaST.pop();
                    ++stCrt;
                    delta.push_back(Tranzitie(stCrt, 'E', M1.initial));
                    delta.push_back(Tranzitie(M1.final, 'E', M1.initial));
                    delta.push_back(Tranzitie(M1.final, 'E', stCrt + 1));
                    delta.push_back(Tranzitie(stCrt, 'E', stCrt + 1));
                    stivaST.push(SistemTranzitional(stCrt, stCrt + 1));
                    ++stCrt;
                    break;
            }
        }
    }

    M1 = stivaST.top();
    stivaST.pop();

    //setare Q
    vector<int> q(stCrt + 1);
    for (int i = 0; i <= stCrt; ++i)
    {
        q[i] = i;
    }
    //setare Q0
    int q0 = M1.initial;
    //setare F
    vector<int> f;
    f.push_back(M1.final);

    return Automat(q, sigma, q0, f, delta);
}

int main()
{
    string w1 = "((a+bc)*bc(d+ab)#)*";
    vector<char> sigma = {'a', 'b', 'c', 'd'};
    string w2 = pas1(w1, sigma);
    cout << "w2 = " << w2 <<"\n";
    string w3 = pas2(w2, sigma);
    cout << "w3 = " << w3 << "\n";
    Automat a = pas3(w3, sigma);
    cout << "Automatul obtinut este: \n";
    a.afisare();
    return 0;
}
