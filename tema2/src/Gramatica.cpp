#include "Gramatica.h"

#include <iostream>
#include <fstream>

using std::vector;
using std::string;
using std::cout;
using std::ifstream;

void Productii::adaugaProductie(const Productie& p)
{
    productii_.push_back(p);
}

bool terminal(std::string simbol)
{
    unsigned int charIdx;
    for (charIdx = 0; charIdx < simbol.size(); ++charIdx)
    {
        if (simbol[charIdx] < 'a' || simbol[charIdx] > 'z')
        {
            return false;
        }
    }

    return true;
}

std::istream& operator >> (std::istream& is, Productie& p)
{
    //stergem toate datele care
    //erau inainte in productie
    p.stanga_ = "";
    p.dreapta_.resize(0);
    //citire simbol stanga
    is >> p.stanga_;
    //citire simbol dreapta
    unsigned int nrSimboluriDreapta;
    is >> nrSimboluriDreapta;
    unsigned int simbolIdx;
    for (simbolIdx =  0; simbolIdx < nrSimboluriDreapta; ++simbolIdx)
    {
        //citim cate un grup de simboluri intr-un string
        //De exemplu: A->aC vom avea
        //grupSimboluri = "aC"
        std::string grupSimboluri;
        is >> grupSimboluri;
        //fiecare litera din grup va fi transformata intr-un
        //string si adaugata la vectorul final
        std::vector<std::string> vectorSimboluri;
        for (unsigned int simbolGrupIdx = 0;
             simbolGrupIdx < grupSimboluri.size();
             ++simbolGrupIdx)
        {
            string simbol = string(1, grupSimboluri[simbolGrupIdx]);
            vectorSimboluri.push_back(simbol);
        }
        p.dreapta_.push_back(vectorSimboluri);
    }

    return is;
}
bool afisareProductie(string stanga, vector<string> dreapta)
{
    //functie care afiseaza fieacare productie
    //intoarce false deoarece e folosita in
    //metoda filtreazaDreapta a clasei productie
    //si metoda necesita o valoare de retur
    cout << stanga;
    cout << " -> ";
    unsigned int dreaptaIdx;
    for (dreaptaIdx = 0; dreaptaIdx < dreapta.size(); ++dreaptaIdx)
    {
        cout << dreapta[dreaptaIdx];
    }
    cout << "\n";
    return false;
}
void Gramatica::afisare()
{
    //afisare N
    cout << "N = {";
    unsigned int simbolIdx;
    for (simbolIdx = 0; simbolIdx < N_.size(); ++simbolIdx)
    {
        cout << N_[simbolIdx] << ", ";
    }
    cout << "}\n";

    //afisare sigma
    cout << "sigma = {";
    for (simbolIdx = 0; simbolIdx < sigma_.size(); ++simbolIdx)
    {
        cout << sigma_[simbolIdx] << ", ";
    }
    cout << "}\n";

    //afisare productii
    cout << "P\n";
    vector<Productie> productii = P_.productii();
    unsigned int productieIdx;
    for (productieIdx = 0;
         productieIdx < productii.size();
         ++productieIdx)
    {
        productii[productieIdx].filtreazaDreapta(afisareProductie);
    }
    cout << "\n";

    //afisare S
    cout << "S = {";
    for (simbolIdx = 0; simbolIdx < S_.size(); ++simbolIdx)
    {
        cout << S_[simbolIdx] << ", ";
    }
    cout <<"}\n";
}

void Gramatica::citireDinFisier(std::string fileName)
{
    ifstream f(fileName, ifstream::in);

    //citire simboluri neterminale
    unsigned int count;
    f >> count;
    N_.resize(count);
    unsigned int countIdx;
    for (countIdx = 0; countIdx < count; ++countIdx)
    {
        f >> N_[countIdx];
    }

    //citire simboluri terminale 
    f  >> count;
    sigma_.resize(count);
    for (countIdx = 0; countIdx < count; ++countIdx)
    {
        f >> sigma_[countIdx];
    }

    //citire simbol de start
    f >> S_;

    //citire reguli productie
    f >> count;
    Productie p;
    for (countIdx = 0; countIdx < count; ++countIdx)
    {
        f >> p;
        P_.adaugaProductie(p);
    }
}
