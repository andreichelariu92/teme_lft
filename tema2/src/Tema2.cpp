#include "Tema2.h"

#include <algorithm>

using std::string;
using std::vector;

bool esteNeterminal(string stanga, 
                    vector<string> dreapta)
{
    const char primaLitera = (dreapta[0])[0];

    if (primaLitera >= 'A'
        &&
        primaLitera <= 'Z')
    {
        return true;
    }
    else
    {
        return false;
    }
}

vector<string> concatenareVectori(vector<string>& v1,
                                  vector<string>& v2)
{
    vector<string> output(v1.size() + v2.size(), "");
    vector<string>::iterator lastV1 = 
        std::copy(v1.begin(), v1.end(), output.begin());

    std::copy(v2.begin(), v2.end(), lastV1);

    return output;
}

bool operator!=(Tranzitie& t1, Tranzitie& t2)
{
    if (t1.simbol != t2.simbol ||
        t1.varfStiva != t2.varfStiva ||
        t1.continutStiva != t2.continutStiva)
    {
        return true;
    }
    else
    {
        return false;
    }
}
vector<Tranzitie> Delta::filtreazaTranzitii(string stare,
                                             string simbol,
                                             string varfStiva)
{
    vector<Tranzitie> output;
    vector<Tranzitie>::iterator tranzitieIt;
    
    for (tranzitieIt = tranzitii_.begin();
         tranzitieIt != tranzitii_.end();
         ++tranzitieIt)
    {
        if ((*tranzitieIt).stareStart == stare &&
            (*tranzitieIt).simbol == simbol &&
            (*tranzitieIt).varfStiva == varfStiva)
        {
            output.push_back((*tranzitieIt));
        }
    }

    return output;
}

void Configuratie::avansare()
{
    cuvant_.erase(0, 1);
    stiva_.pop();
}

void Configuratie::expandare(vector<string>& v)
{
    stiva_.pop();

    int vIdx;
    for (vIdx = v.size() - 1; vIdx >= 0; --vIdx)
    {
        stiva_.push(v[vIdx]);
    }
}
