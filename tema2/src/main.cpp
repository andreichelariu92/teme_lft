#include <iostream>
#include <algorithm>

#include "Gramatica.h"
#include "Tema2.h"

using std::cout;
using std::string;
using std::cin;
using std::vector;

int main()
{
    Gramatica g;
    g.citireDinFisier("./input.txt");
    g.afisare();
    
    string Q = "q";
    vector<string> sigma = g.sigma();
    vector<string> N = g.N();
    vector<string> gamma = 
        concatenareVectori(N, sigma);
    string Z0 = g.S();
    vector<Tranzitie> tranzitii;

    //completare functie de tranzitie
    vector<string> epsilon(1, "epsilon");
    unsigned int sigmaIdx;
    for (sigmaIdx = 0; sigmaIdx < sigma.size(); ++sigmaIdx)
    {
        Tranzitie t(Q, sigma[sigmaIdx], sigma[sigmaIdx], Q, epsilon);
        tranzitii.push_back(t);
    }
    vector<Productie> productii = g.P().productii();
    vector<Productie>::iterator prodIt;
    for (prodIt = productii.begin();
         prodIt != productii.end();
         ++prodIt)
    {
        MatriceString_t dreapta = (*prodIt).dreapta();
        MatriceString_t::iterator dreaptaIt;
        
        for (dreaptaIt = dreapta.begin();
             dreaptaIt != dreapta.end();
             ++dreaptaIt)
        {
            Tranzitie t(Q, 
                        "epsilon", 
                        (*prodIt).stanga(),
                        Q,
                        *dreaptaIt);
            tranzitii.push_back(t);
        }
    }
    Delta delta(tranzitii);
    /*
    for (auto t : tranzitii)
    {
        cout << "delta(" << t.stareStart
             << "," << t.simbol << ","
             << t.varfStiva
             << ") = ("
             << t.stareNoua
             << ", ";
        for (auto cs : t.continutStiva)
        {
            cout << cs;
        }

        cout <<")\n";
    }
    */
    string cuvant("i+i*i");
    /*
    cout << "Introduceti cuvantul\n";
    cin >> cuvant;
    */
    //validare cuvant
    Configuratie conf(Q, cuvant, g.S());
    Tranzitie tranzitiePrecedenta;
    while (conf.stiva().empty() == false)
    {
        const string simbolCuvant = string(1, conf.cuvant()[0]);
        vector<Tranzitie> avans =
            delta.filtreazaTranzitii(Q, 
                                    simbolCuvant,
                                    conf.stiva().top());

        if (avans.size() != 0)
        {
            conf.avansare();
            tranzitiePrecedenta = avans[0];
        }
        else
        {
            vector<Tranzitie> expandare =
                delta.filtreazaTranzitii(Q, 
                                         "epsilon",
                                         conf.stiva().top());
            //cautam o tranzitie care sa ne
            //permita expandare la urmatoarea
            //iteratie
            const string s(1, conf.cuvant()[0]);
            ConditieAvansare ca(s);
            vector<Tranzitie>::iterator tranzitieIt;
            tranzitieIt = std::find_if(expandare.begin(),
                                       expandare.end(),
                                       ca);
            if (tranzitieIt == expandare.end())
            {
                //daca nu am gasit o tranzitie de
                //avansare,
                //cautam prima tranzitie diferita
                //de cea precedenta din vector
                ConditieTranzitie ct(tranzitiePrecedenta);
                tranzitieIt = std::find_if(expandare.begin(),
                                           expandare.end(),
                                           ct);
            }
            
            //daca nu am gasit nici o tranzite
            //valida intrerupem algoritmul
            if (tranzitieIt == expandare.end())
                break;

            conf.expandare((*tranzitieIt).continutStiva);
            tranzitiePrecedenta = (*tranzitieIt);
        }
    }

    if (conf.stiva().empty() == true)
        cout << "Cuvantul este acceptat\n";
    else
        cout << "Cuvantul nu este acceptat\n";
    
    return 0;
}
