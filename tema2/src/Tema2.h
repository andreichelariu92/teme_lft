#ifndef Tema2_H_INCLUDE_GUARD
#define Tema2_H_INCLUDE_GUARD

#include <string>
#include <vector>
#include <stack>

bool esteNeterminal(std::string stanga, 
                    std::vector<std::string> dreapta);


struct Tranzitie
{
    std::string stareStart;
    std::string simbol;
    std::string varfStiva;
    std::string stareNoua;
    std::vector<std::string> continutStiva;
    
    Tranzitie()
        :stareStart(),
         simbol(),
         varfStiva(),
         stareNoua(),
         continutStiva()
    {}

    Tranzitie(std::string argStareStart,
              std::string argSimbol,
              std::string argVarfStiva,
              std::string argStareNoua,
              std::vector<std::string> argContinutStiva)
        :stareStart(argStareStart),
         simbol(argSimbol),
         varfStiva(argVarfStiva),
         stareNoua(argStareNoua),
         continutStiva(argContinutStiva)
    {}
};

bool operator!= (Tranzitie& t1, Tranzitie& t2);

std::vector<std::string> concatenareVectori(std::vector<std::string>& v1, std::vector<std::string>& v2);

class Delta
{
private:
    std::vector<Tranzitie> tranzitii_;
public:
    Delta(std::vector<Tranzitie> tranzitii)
        :tranzitii_(tranzitii)
    {}

    std::vector<Tranzitie> filtreazaTranzitii(std::string stare,
                                              std::string simbol,
                                              std::string varfStiva);
};

class Configuratie
{
private:
    std::string stare_;
    std::string cuvant_;
    std::stack<std::string> stiva_;
public:
    Configuratie(std::string argStare,
                 std::string argCuvant,
                 std::string varfStiva)
        :stare_(argStare),
         cuvant_(argCuvant),
         stiva_()
    {
        stiva_.push(varfStiva);
    }

    std::string stare()
    {
        return stare_;
    }

    std::string cuvant()
    {
        return cuvant_;
    }

    std::stack<std::string>& stiva()
    {
        return stiva_;
    }

    void avansare();

    void expandare(std::vector<std::string>& v);
};

struct ConditieTranzitie
{
    Tranzitie tranzitie;
    ConditieTranzitie(Tranzitie argT)
        :tranzitie(argT)
    {}

    bool operator()(Tranzitie t)
    {
        if (tranzitie != t)
            return true;
        else
            return false;
    }
};

struct ConditieAvansare
{
    std::string simbol;

    ConditieAvansare(std::string argSimbol)
        :simbol(argSimbol)
    {}

    bool operator()(Tranzitie t)
    {
        if (t.continutStiva[0] == simbol)
            return true;
        else
            return false;
    }
};
#endif
