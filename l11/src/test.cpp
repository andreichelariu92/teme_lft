#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <stack>
#include <fstream>

using std::map;
using std::string;
using std::vector;
using std::stack;
using std::ifstream;

typedef map<string, map<string, vector<string> > > ParsingTable_t;

bool validateWord(ParsingTable_t table, 
                  vector<string> word, 
                  string startSimbol,
                  string stopSimbol)
{
    stack<string> stack;
    stack.push(startSimbol);
    
    unsigned int currentIdx = 0;
    while (true)
    {
        //the stack should only be empty
        //if we reached the stopSimbol
        if (stack.empty())
            break;

        string topStack = stack.top();
        stack.pop();
        
        if (word[currentIdx] == topStack)
        {
            ++currentIdx;
        }
        else
        {
            //get the productions for the pair
            //of stack simbol and word simbol
            map<string, vector<string> >& topStackRules 
                = table[topStack];
            vector<string>& production = 
                topStackRules[word[currentIdx]];

            if (production[0] == "invalid")
            {
                break;
            }
            else if (production[0] != "epsilon")
            {
                //insert the simbols from the
                //production in reverse order
                int productionIdx = 0;
                for (productionIdx = production.size() - 1;
                     productionIdx >= 0;
                     --productionIdx)
                {
                    stack.push(production[productionIdx]);
                }
            }
        }
    }

    if (word[currentIdx] == stopSimbol
        &&
        stack.empty())
    {
        return true;
    }
    else
    {
        return false;
    }
}

ParsingTable_t readParsingTable(string fileName)
{
   ifstream f(fileName, std::ios_base::in);
   
   //read the neterminals
   unsigned int nrNeterminals = 0;
   f >> nrNeterminals;
   vector<string> neterminals(nrNeterminals, "");
   unsigned int neterminalIdx;
   for (neterminalIdx = 0; 
        neterminalIdx < nrNeterminals;
        ++neterminalIdx)
   {
        f >> neterminals[neterminalIdx];
   }

   //read the terminals
   unsigned int nrTerminals = 0;
   f >> nrTerminals;
   vector<string> terminals(nrTerminals, "");
   unsigned int terminalIdx;
   for (terminalIdx = 0;
        terminalIdx < nrTerminals;
        ++terminalIdx)
   {
       f >> terminals[terminalIdx];
   }

   //read the productions
   ParsingTable_t output;
   for (neterminalIdx = 0;
        neterminalIdx < nrNeterminals;
        ++neterminalIdx)
   {
       map<string, vector<string> > productionMap;

       for (terminalIdx = 0;
            terminalIdx < nrTerminals;
            ++terminalIdx)
       {
           unsigned int nrProductions;
           f >> nrProductions;
           vector<string> productions(nrProductions, "");
           unsigned int productionIdx;
           for (productionIdx = 0;
                productionIdx < nrProductions;
                ++productionIdx)
           {
               f >> productions[productionIdx];
           }

           //map the production vector with
           //the current terminal
           productionMap[terminals[terminalIdx]] = productions;
       }

       output[neterminals[neterminalIdx]] = productionMap;
   }

   return output;
}

int main()
{
    ParsingTable_t table = readParsingTable("./input.txt");
    vector<string> word;
    word.push_back("int");
    word.push_back("+");
    word.push_back("int");
    word.push_back("*");
    word.push_back("int");
    word.push_back("$");
    bool valid = validateWord(table, word, "E", "$");
    std::cout << valid << "\n";
    return 0;
}
