#include <iostream>
#include <algorithm>

#include "Automat.h"

using std::string;
using std::vector;
using std::cout;

int main()
{
    Automat a1;
    a1.citesteDinFisier("./input1.txt");
    //a1.afisare();
    //cout << "\n";

    Automat a2;
    a2.citesteDinFisier("./input2.txt");
    //a2.afisare();
    //cout << "\n";
    
    bool echivalente = (a1 == a2);
    if (echivalente)
    {
        cout << "Cele 2 automate sunt echivalente\n";
    }
    else
    {
        cout << "Cele 2 automate nu sunt echivalente\n";
    }
    return 0;
}
