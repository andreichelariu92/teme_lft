#include "Automat.h"

#include <fstream>
#include <iostream>
#include <set>
#include <algorithm>

using std::vector;
using std::pair;
using std::cout;
using std::string;
using std::set;
using std::map;

FunctieDeTranzitie::FunctieDeTranzitie()
    :tranzitii_()
{}

FunctieDeTranzitie::FunctieDeTranzitie
                    (vector<Tranzitie> tranzitii)
    :tranzitii_(tranzitii)
{}

int FunctieDeTranzitie::efectueazaTranzitie
                        (int stare, char simbol, string mesaj)
{
    vector<Tranzitie>::iterator it;
    //stareRezultat este -1, marcata
    //ca nevalida, daca se va gasi o stare
    //in vector pentru care se va putea face
    //tranzitia, valoarea lui stareRezultat
    //va fi actualizata
    int stareRezultat = -1;
    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare &&
            (*it).simbol == simbol)
        {
            stareRezultat = (*it).stareFinala;
        }
    }
    
    //se afiseaza mesajul doar daca trazitia
    //e valida si mesajul contine ceva
    if (mesaj != "")
    {
        std::cout << mesaj;
        std::cout << "~~~~~~~~~~~~~~~~~~~~\n";
    }

    return stareRezultat;
}

vector<pair<char, int> > FunctieDeTranzitie::gasesteTranzitii
                         (int stare)
{
    vector<pair<char, int> > tranzitiiPosibile;
    vector<Tranzitie>::iterator it;

    for (it = tranzitii_.begin();
         it != tranzitii_.end();
         ++it)
    {
        if ((*it).stareInitiala == stare)
        {
            pair<char, int> p((*it).simbol, (*it).stareFinala);
            tranzitiiPosibile.push_back(p);
        }
    }

    return tranzitiiPosibile;
}

vector<int> FunctieDeTranzitie::epsilonInchidere(int stare)
{
    vector<int> output;
    output.push_back(stare);
    vector<Tranzitie>::iterator itTranzitie;
    
    for (itTranzitie = tranzitii_.begin();
         itTranzitie != tranzitii_.end();
         ++itTranzitie)
    {
        if ((*itTranzitie).stareInitiala == stare &&
            (*itTranzitie).simbol == 'E')
        {
            output.push_back((*itTranzitie).stareFinala);
        }

    }
    
    return output;
}
vector<int> FunctieDeTranzitie::efectueazaTranzitieMultime(int stare,
                                                           char simbol)
{
    vector<int> output;

    vector<Tranzitie>::iterator itTranzitie;
    for (itTranzitie = tranzitii_.begin();
         itTranzitie != tranzitii_.end();
         ++itTranzitie)
    {
        if ((*itTranzitie).stareInitiala == stare &&
            (*itTranzitie).simbol == simbol)
        {
            output.push_back((*itTranzitie).stareFinala);
        }
    }

    return output;
}

void Automat::citesteDinFisier(std::string fileName)
{
    std::ifstream f(fileName.c_str(), std::ifstream::in);

    //citire stari
    int nrStari = 0;
    f >> nrStari;
    q_.resize(nrStari);
    for (int stareIdx = 0; stareIdx < nrStari; ++stareIdx)
        f >> q_[stareIdx];
    //citire simboluri
    int nrSimboluri = 0;
    f >> nrSimboluri;
    sigma_.resize(nrSimboluri);
    for (int simbolIdx = 0; 
         simbolIdx < nrSimboluri; 
         ++simbolIdx)
    {
        f >> sigma_[simbolIdx];
    }
    //citire stare initiala
    f >> q0_;
    //citire stari finale
    int nrStariFinale = 0;
    f >> nrStariFinale;
    f_.resize(nrStariFinale);
    for (int stareFIdx = 0; 
         stareFIdx < nrStariFinale; 
         ++stareFIdx)
    {
        f >> f_[stareFIdx];
    }
    //citire functie tranzitie
    int stareInitiala;
    char simbol;
    int stareFinala;
    vector<Tranzitie> tranzitii;
    while (f >> stareInitiala >> simbol >> stareFinala)
    {
        Tranzitie t(stareInitiala, simbol, stareFinala);
        tranzitii.push_back(t);
    }
    delta_ = FunctieDeTranzitie(tranzitii);
}

void Automat::afisare()
{
    cout << "Q = {";
    for (unsigned int i = 0; i < q_.size(); ++i)
        std::cout << q_[i] << ", ";
    std::cout << "}\n";
    
    std::cout << "Sigma = {";
    for (unsigned int i = 0; i < sigma_.size(); ++i)
    {
        std::cout << sigma_[i] << ", ";
    }
    std::cout << "}\n";
    
    std::cout << "Q0 = ";
    std::cout << q0_ << "\n";
    
    std::cout << "F = {";
    for (unsigned int i = 0; i < f_.size(); ++i)
    {
        std::cout << f_[i] << ", ";
    }
    std::cout << "}\n";
    
    std::cout << "Delta(functia de tranzitie):\n";
    std::vector<Tranzitie>& tranzitii = delta_.tranzitii();
    for (unsigned int i = 0; i < tranzitii.size(); ++i)
    {
        std::cout << tranzitii[i].stareInitiala << " ";
        std::cout << tranzitii[i].simbol << " ";
        std::cout << tranzitii[i].stareFinala << "\n";
    }
    std::cout << "\n";
}
vector<int> Automat::stariAccesibile()
{
    vector<int> output;
    for (unsigned int qIdx = 0; qIdx < q_.size(); ++qIdx)
    {
        if (q_[qIdx] != q0_)
        {
            string drum = Dijkstra(*this, q0_, q_[qIdx]);
            if (drum != infinit())
            {
                output.push_back(q_[qIdx]);
            }
        }
    }

    return output;
}
vector<int> Automat::stariFinalizate()
{
    //caut daca exista un drum de la
    //fiecare stare la una din starile finale,
    //daca da le adaug in vector
    vector<int> output;
    for (unsigned int qIdx = 0; qIdx < q_.size(); ++qIdx)
    {
        for (unsigned int fIdx = 0; fIdx < f_.size(); ++fIdx)
        {
            string drum = Dijkstra(*this, q_[qIdx], f_[fIdx]);
            if (drum != infinit())
            {
                output.push_back(q_[qIdx]);
                //daca am gasit deja un drum de la starea
                //curenta spre o stare finala nu mai are rost
                //sa mai cautam pentru celelalte stari finale
                break;
            }
        }
    }

    return output;
}
bool limbajRecunoscut(std::string cuvant,
                      Automat& automat,
                      MesajeStari mesajeStari,
                      MesajeTranzitii mesajeTranzitii)
{
    FunctieDeTranzitie& f = automat.delta();
    const unsigned int dimCuvant = cuvant.size();
    int stare= automat.q0();

    for (unsigned int charIdx = 0; charIdx < dimCuvant; ++charIdx)
    {
       //compune mesaj de afisare
       string mesajAfisare;
       if (mesajeStari.find(stare) != mesajeStari.end())
       {
           mesajAfisare += mesajeStari[stare];
       }
       if (mesajeTranzitii.find(cuvant[charIdx]) != mesajeTranzitii.end())
       {
           mesajAfisare += mesajeTranzitii[cuvant[charIdx]];
       }
       //se efectueaza cate o tranzitie pentru fiecare
       //simbol(litera) a cuvantului
       //daca se ajunge intr-o stare invalida(nu se poate
       //face tranzitie), atunci se returneaza false 
       stare = f.efectueazaTranzitie(stare, cuvant[charIdx], mesajAfisare); 
       if (stare == -1)
       {
           //afiseaza starea finala inainte
           //de a parasi functia
           if (mesajeStari.find(stare) != mesajeStari.end())
           {
               cout << mesajeStari[stare];
           }
           return false;
       }
    }

    //se verifica daca starea in care am ajuns dupa efectuarea
    //tututor tranzitiilor face parte din multimea starilor
    //finale
    vector<int>& stariFinale = automat.f();
    const unsigned int dimStariFinale = stariFinale.size();

    for (unsigned int stariFinaleIdx = 0; 
         stariFinaleIdx < dimStariFinale;
         ++stariFinaleIdx)
    {
        if (stare == stariFinale[stariFinaleIdx])
        {
           if (mesajeStari.find(stare) != mesajeStari.end())
           {
               cout << mesajeStari[stare];
           }
           return true;
        }
    }

    //daca starea finala in care am ajuns nu se gaseste
    //in multimea starilor finale, se returneaza false
    if (mesajeStari.find(stare) != mesajeStari.end())
    {
        cout << mesajeStari[stare];
    }
    return false;
}

string infinit()
{
    string s;
    s += static_cast<char>(255);
    return s;
}

bool conditieDeStop(map<int, string>& distanta,
                    set<int, Comparator>& stariNevizitate)
{
    //daca setul e vid se intoarce true
    if (stariNevizitate.begin() ==
        stariNevizitate.end())
    {
        return true;
    }

    //se verifica daca prima stare din cele nevizitate
    //are distanta infinit
    const int primaStareNevizitata = *(stariNevizitate.begin());
    if (distanta[primaStareNevizitata] == 
        infinit())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//se actualizeaza starile nevizitate
//prin stergerea si adaugarea starii modificate
//in acest fel se reorganizeaza setul
void actualizeazaStariNevizitate
     (set<int, Comparator>& stariNevizitate,
      int stareModificata)
{
    stariNevizitate.erase(stareModificata);
    stariNevizitate.insert(stareModificata);
}

//calculeaza cel mai scurt drum de la stareaInitiala la cea finala
//daca nu exista drum, atunci va returna litera '255'
//daca starile coincid, atunci va returna cuvantul vid ""
string Dijkstra(Automat& automat, int stareInitiala, int stareFinala)
{
    //INSPIRATIE: 
    //https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm 
    
    //map ce asociaza fiecarei stari din automat o distanta
    map<int, string> distanta;
    //set care memoreaza starile nevizitate in ordine crescatoare,
    //pe baza distantei pana la ele
    Comparator comparator(distanta);
    set<int, Comparator> stariNevizitate(comparator);

    //initializare structuri de date
    distanta[stareInitiala] = "";
    stariNevizitate.insert(stareInitiala);
    std::vector<int>& stari = automat.q();
    for (unsigned int stareIdx = 0; 
         stareIdx < stari.size(); 
         ++stareIdx)
    {
        if (stari[stareIdx] != stareInitiala)
        {
            //asociem fiecarei stari valoarea infinit pentru
            //distanta
            distanta[ stari[stareIdx] ] = infinit();
            //adaugam toate starile, in afara de cea initiala
            //in stariNevizitate
            stariNevizitate.insert(stari[stareIdx]);
        }
    }

    int stareCurenta = stareInitiala;
    bool exit = false;
    while (!exit)
    {
        //luam toate starile in care putem
        //ajunge din starea curenta
        vector<pair<char, int> > tranzitii =
            automat.delta().gasesteTranzitii(stareCurenta);
        //parcurg toate tranzitiile vecine
        vector<pair<char, int> >::iterator itTranzitie;
        for (itTranzitie = tranzitii.begin();
             itTranzitie != tranzitii.end();
             ++itTranzitie)
        {
            //calcul distanta tentativa
            string distantaTentativa = distanta[stareCurenta];
            distantaTentativa += (*itTranzitie).first;
            //daca distanta tentativa este mai mica decat
            //cea din map se actualizeaza distanta
            if (distanta[(*itTranzitie).second] >
                distantaTentativa)
            {
                distanta[(*itTranzitie).second] =
                    distantaTentativa;

                actualizeazaStariNevizitate
                (stariNevizitate,
                 (*itTranzitie).second);
            }
        }
        //scot starea curenta din stariNevizitate
        stariNevizitate.erase(stareCurenta);
        exit = conditieDeStop(distanta, stariNevizitate);
        //se alege ca stare curenta prima stare din set
        if (!exit)
        {
            stareCurenta = *(stariNevizitate.begin());
        }
    }

    return distanta[stareFinala];
}

vector<int> reuniuneVectori(vector<vector<int> > vectori)
{
    //INSPIRATION:
    //http://stackoverflow.com/questions/29968206/how-to-find-union-of-n-vector

    set<int> reuniune;
    //adauga toti vectorii in set
    //setul va face sortarea lor,
    //iar elementele duplicat nu vor fi adaugate
    for (unsigned int idxVector = 0;
         idxVector != vectori.size();
         ++idxVector)
    {
        reuniune.insert(vectori[idxVector].begin(),
                        vectori[idxVector].end());
    }

    //pune toate valorile din set intr-un vector
    vector<int> output(reuniune.begin(), reuniune.end());

    return output;
}

int stareCompusa(std::vector<int> stari)
{
    if (stari.size() == 0)
        return -1;

    int output = 0;
    for (unsigned int idxStare = 0;
         idxStare < stari.size();
         ++idxStare)
    {
        output = output * 10 + stari[idxStare];
    }

    return output;
}

typedef vector<vector<int> > matrice;

bool cautaVector(vector<int>& v, int val)
{
    vector<int>::iterator pozitie;
    pozitie = std::find(v.begin(), v.end(), val);
    if (pozitie != v.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}

vector<int> diferentaVectori(vector<int>& a, vector<int>& b)
{
    vector<int> output;
    for (unsigned int aIdx = 0; aIdx < a.size(); ++aIdx)
    {
        if (cautaVector(b, a[aIdx]) == false)
        {
            output.push_back(a[aIdx]);
        }
    }

    return output;
}
vector<int> eliminareStari(Automat& automat)
{
    vector<int> output;
    vector<int> stariAccesibile = automat.stariAccesibile();
    vector<int> stariFinalizate = automat.stariFinalizate();
    vector<int>& automatQ = automat.q();
    //adaug in vectorul de iesire doar
    //starile care fac parte din cele
    //accesibile si finalizate
    for (unsigned int qIdx = 0;
         qIdx < automatQ.size();
         ++qIdx)
    {
        if (cautaVector(stariAccesibile, automatQ[qIdx]) &&
            cautaVector(stariFinalizate, automatQ[qIdx]))
        {
            output.push_back(automatQ[qIdx]);
        }
    }

    return output;
}

Automat minimizareAutomat(Automat& automat)
{
    //extragem datele din automat
    vector<int>& f = automat.f();
    vector<int> q = automat.q();
    vector<int> qMinusF = diferentaVectori(q, f);
    vector<char> sigma = automat.sigma();
    FunctieDeTranzitie delta = automat.delta();
    int q0 = automat.q0();

    //initializare matrice
    matrice A(automat.q().size());
    for (unsigned int i = 0; i < A.size(); ++i)
    {
        //presupunem la inceput ca toate elementele
        //sunt indistinctibile intre ele
        A[i] = vector<int>(automat.q().size());
    }

    //marcam ca distinctibile elementele care fac parte
    //din clase diferite de stari(nefinale si finale)
    for (unsigned int i = 0; i < q.size() - 1; ++i)
    {
        for (unsigned int j = i+1; j < q.size(); ++j)
        {
            if ((cautaVector(f, q[i]) && cautaVector(qMinusF, q[j]))
                ||
                (cautaVector(qMinusF, q[i]) && cautaVector(f, q[j])))
            {
                A[i][j] = 1;
                A[j][i] = 1;
            }
        }
    }
    
    bool modif = false;
    do
    {
        modif = false;
        for (unsigned int i = 0; i < q.size(); ++i)
        {
            for (unsigned int j = 0; j < q.size(); ++j)
            {
                if (A[i][j] == 0)
                {
                    for (unsigned int sigmaIdx = 0;
                         sigmaIdx < sigma.size();
                         ++sigmaIdx)
                    {
                        int tranzitieQI = delta.efectueazaTranzitie(q[i], sigma[sigmaIdx]);
                        int tranzitieQJ = delta.efectueazaTranzitie(q[j], sigma[sigmaIdx]);
                        //daca doar una din tranzitii nu este valida
                        //,atunci starile sunt distinctibile
                        if ((tranzitieQI < 0 && tranzitieQJ > 0)
                            ||
                            (tranzitieQI > 0 && tranzitieQJ < 0))
                        {
                            A[i][j] = 1;
                            A[j][i] = 1;
                            modif = true;
                            //daca am gasit un simbol pentru
                            //care ne dam seama ca starile
                            //sunt distinctibile nu mai
                            //cautam si pentru restul
                            //simbolurilor
                            break;
                        }
                        else
                        {
                            if (tranzitieQI > 0 && tranzitieQJ > 0)
                            {
                                //daca noile stari in care am
                                //ajuns sunt distinctibile,
                                //atunci si starile initiale
                                //sunt distinctibile
                                if (A[tranzitieQI][tranzitieQJ]
                                    != 0)
                                {
                                    A[i][j] = 1;
                                    A[j][i] = 1;
                                    modif = true;
                                    //la fel ca mai sus :)
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

    }while(modif);
    
    //vectorul clasa contine pentru fiecare
    //stare initiala ce stare noua ii va corespunde
    vector<int> clasa(q.size(), -1);
    int nrClase = -1;
    //tine pentru fiecare noua stare din
    //noul automat ce stare din automatul initial
    //ii corespunde
    vector<int> reprezentantClasa;
    //multimea starilor finale din noul
    //automat
    vector<int> fPrim;
    for (unsigned int i = 0; i < q.size(); ++i)
    {
        if (clasa[i] == -1)
        {
            //in nrClase se tine noua stare
            //din noul automat
            clasa[i] = ++nrClase;
            reprezentantClasa.push_back(i);
            if (cautaVector(f, q[i]) == true)
            {
                fPrim.push_back(nrClase);
            }
            //toate starile indistinctibile
            //cu starea q[i] vor face parte
            //din aceeasi clasa
            for (unsigned int j = i + 1; j < q.size(); ++j)
            {
                if (A[i][j] == 0)
                {
                    clasa[j] = nrClase;
                }
            }
        }
    }

    //contruire automat echivalent
    vector<int> qPrim(nrClase + 1);
    vector<Tranzitie> deltaPrim;
    int q0Prim = clasa[q0];

    for (unsigned int i = 0; i < qPrim.size(); ++i)
    {
        qPrim[i] = i;
    }
    for (unsigned int i = 0; i < qPrim.size(); ++i)
    {
        for (unsigned int sigmaIdx = 0; sigmaIdx < sigma.size();
             ++sigmaIdx)
        {
            int nouaStare = delta.efectueazaTranzitie
                (reprezentantClasa[i], sigma[sigmaIdx]);
            if (nouaStare != -1)
            {
                Tranzitie t(i, sigma[sigmaIdx], clasa[nouaStare]);
                deltaPrim.push_back(t);
            }
        }
    }

    Automat automatMinimizat(qPrim, sigma, 
            q0Prim, fPrim, deltaPrim);
    
    return automatMinimizat;
}

bool operator==(Automat a1, Automat a2)
{
    //verificam daca ambele automate
    //sunt definite pe acelasi alfabet
    if (comparaVectori(a1.sigma(), a2.sigma()) == false)
    {
        return false;
    }

    Automat a1Min= minimizareAutomat(a1);
    Automat a2Min= minimizareAutomat(a2);

    if (a1Min.q().size() != a2Min.q().size())
    {
        return false;
    }
    //daca automatele nu au acelasi numar de tranzitii,
    //atunci ele nu sunt echivalente
    if (a1Min.delta().tranzitii().size() != 
            a2Min.delta().tranzitii().size())
    {
        return false;
    }
    if (a1Min.f().size() != a2Min.f().size())
    {
        return false;
    }

    return renumerotareStari(a1, a2);
}


bool renumerotareStari(Automat& a1, Automat& a2)
{
    //structuri de date utilizate in algoritm
    
    //asociaza fiecarei stari din a1 starea
    //corespunzatoare din a2
    map<int, int> mapA1A2;
    //mentine multimea starilor
    //vizitate
    set<int> stariNevizitate;
    
    //luam informatiile din cele 2 automate
    FunctieDeTranzitie& delta1 = a1.delta();
    FunctieDeTranzitie& delta2 = a2.delta();
    //introducem starile de start
    //in structurile de date
    mapA1A2.insert(std::make_pair(a1.q0(), a2.q0()));
    stariNevizitate.insert(a1.q0());

    bool ok = true;
    
    while (stariNevizitate.size() != 0)
    {

        int stareCurentaA1 = *(stariNevizitate.begin());
        stariNevizitate.erase(stariNevizitate.begin());
        int stareCurentaA2 = mapA1A2[stareCurentaA1];

        if (stareCurentaA1 == -1 || stareCurentaA2 == -1)
        {
            ok = false;
            break;
        }

        vector<pair<char, int> > tranzitiiPosibileA1 =
            delta1.gasesteTranzitii(stareCurentaA1);

        //pentru fiecare tranzitie din stareCurentaA1 verific
        //daca exista o tranzitie cu acelasi simbol din
        //stareCurentaA2
        vector<pair<char, int> >::iterator tranzitieIt;
        for (tranzitieIt = tranzitiiPosibileA1.begin();
             tranzitieIt != tranzitiiPosibileA1.end();
             ++tranzitieIt)
        {
            const int stareNouaA2 = 
                delta2.efectueazaTranzitie(stareCurentaA2,
                                           (*tranzitieIt).first);
            const int stareNouaA1 = (*tranzitieIt).second;
            
            map<int, int>::iterator mapIt = mapA1A2.find(stareNouaA1);
            
            //daca nu am vizitat noua stare,
            //o adaugam la starile nevizitate,
            if (mapIt == mapA1A2.end())
            {
               stariNevizitate.insert(stareNouaA1);
               //adaugam asocierea dintre noua stare din a1
               //si noua stare din a2 in map
               mapA1A2.insert(std::make_pair(stareNouaA1, 
                                             stareNouaA2));
            }
            else
            {
                //daca am vizitat deja starea,
                //verificam daca asocierea facuta atunci
                //se respecta si pentru tranzitia curenta
                if ((*mapIt).second != stareNouaA2)
                {
                    ok = false;
                    break;
                }
            }
        }
    }

    return ok;
}
