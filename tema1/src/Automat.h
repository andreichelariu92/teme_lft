#ifndef Automat_H_INCLUDE_GUARD
#define Automat_H_INCLUDE_GUARD

#include <vector>
#include <string>
#include <utility>
#include <map>

typedef std::map<int, std::string> MesajeStari;
typedef std::map<char, std::string> MesajeTranzitii;

///structura ce defineste o
///tranzitie din starea initiala
///in starea finala folosind
///simbolul "simbol"
struct Tranzitie
{
    int stareInitiala;
    char simbol;
    int stareFinala;

    Tranzitie(int sI, char s, int sF)
        :stareInitiala(sI),
         simbol(s),
         stareFinala(sF)
    {}

    Tranzitie()
        :stareInitiala(0),
         simbol(0),
         stareFinala(0)
    {}
};

///clasa care defineste matricea de
///tranzitie a unui automat finit determinist
///ofera in plus metode ajutatoare
class FunctieDeTranzitie
{
    private:
        std::vector<Tranzitie> tranzitii_;
    public:
        FunctieDeTranzitie();
        FunctieDeTranzitie(std::vector<Tranzitie> tranzitii);
        
        //efectueaza o tranzitie din starea "stare" cu
        //ajutorul simbolului simbol
        //intoarce noua stare dupa tranzitie sau -1
        //daca nu se poate face tranzitia
        int efectueazaTranzitie(int stare,
                                char simbol,
                                std::string mesaj = "");
        
        ///intoarce un vector cu tranzitiile posibile
        ///din starea "stare" cu simbolul "simbol"
        ///valabila doar pentru Automate Nedeterministe
        std::vector<int> efectueazaTranzitieMultime(int stare,
                                                    char simbol);
        ///intoarce un vector cu posibilele tranzitii
        ///din starea "stare"
        ///daca nu exista tranzitii,
        ///vectorul returnat va avea dimensiune 0
        std::vector<std::pair<char, int> > gasesteTranzitii(int stare);
        
        ///returneaza espilon tranzitia unei stari
        std::vector<int> epsilonInchidere(int stare);

        ///metoda de acces la tranizii
        std::vector<Tranzitie>& tranzitii()
        {
            return tranzitii_;
        }
        
};

///clasa ce defineste un automat
///finit determinist
class Automat
{
private:
    //multimea starilor
    std::vector<int> q_;
    //multimea simbolurilor
    std::vector<char> sigma_;
    //starea initiala
    int q0_;
    //multimea starilor finale
    std::vector<int> f_;
    //functie de tranzitie,
    //definita prin vector de tranzitii
    FunctieDeTranzitie delta_;
public:
    ///constructorul, operatorul= si
    ///destructorul sunt generati implicit
    ///de compilator
    Automat():
        q_(),
        sigma_(),
        q0_(),
        f_(),
        delta_()
    {}
    Automat(const std::vector<int>& q,
            const std::vector<char>& sigma,
            int q0,
            const std::vector<int>& f,
            const std::vector<Tranzitie> tranzitii)
        :q_(q),
         sigma_(sigma),
         q0_(q0),
         f_(f),
         delta_(FunctieDeTranzitie(tranzitii))
    {}
    ///se presupune ca fisierul e scris corect :)
    ///daca nu, atunci comportamentul functiei
    ///e nedefinit
    void citesteDinFisier(std::string fileName);
    
    ///metode de acces la variabilele automatului
    std::vector<int>& q()
    {
        return q_;
    }
    void q(const std::vector<int>& stari)
    {
        q_ = stari;
    }
    void adaugaStare(int s)
    {
        q_.push_back(s);
    }
    std::vector<char>& sigma()
    {
        return sigma_;
    }
    void sigma(std::vector<char> s)
    {
        sigma_ = s;
    }
    int q0()
    {
        return q0_;
    }
    void q0(int q0)
    {
        q0_ = q0;
    }
    std::vector<int>& f()
    {
        return f_;
    }
    void f(const std::vector<int> valoriFinale)
    {
        f_ = valoriFinale;
    }
    FunctieDeTranzitie& delta()
    {
        return delta_;
    }
    void delta(const FunctieDeTranzitie& ft)
    {
        delta_ = ft;
    }
    void adaugaTranzitie(Tranzitie t)
    {
        delta_.tranzitii().push_back(t);
    }
    void afisare();
    ///intoarce un vector cu starile accesibile
    std::vector<int> stariAccesibile();
    ///intoarce un vector cu starile finalizate
    std::vector<int> stariFinalizate();

};

//intoarce true daca prin parcurgerea simbolurilor
//din cuvant ajungem intr-o stare finala
bool limbajRecunoscut(std::string cuvant,
                      Automat& automat,
                      MesajeStari mesajeStari = MesajeStari(),
                      MesajeTranzitii mesajeTranzitii = MesajeTranzitii());

//calculeaza cel mai scurt drum de la stareaInitiala la cea finala
//daca nu exista drum, atunci va returna litera '255'
//daca starile coincid, atunci va returna cuvantul vid ""
std::string Dijkstra(Automat& automat, int stareInitiala, int stareFinala);

//obiect cu ajutorul caruia
//vom tine starile in ordine
//pe baza drumului pana la ele,
//in ordine crescatoare
struct Comparator
{
    //referinta la map-ul din functia de
    //mai jos
    //folosim referinta, pentru a primi
    //modificarile facute pe parcursul
    //algoritmului
    std::map<int, std::string>& distanta;
    //constructor
    Comparator(std::map<int, std::string>& d)
        :distanta(d)
    {}
    //functie care compara doua stari pe
    //baza distantei
    //intoarce true daca stare1 are distanta
    //mai mica si false in caz contrar
    bool operator()(int stare1, int stare2)
    {
        //returnez valoarea intoarsa de operatorul <
        return (distanta[stare1] < distanta[stare2]);
    }
};

///realizeaza reuniunea vectorilor
///acelasi principiu cu reuniunea multimilor
std::vector<int> reuniuneVectori(
        std::vector<std::vector<int> > vectori);

///realizeaza o stare compusa pe baza unui
//vector de stari
//daca vectorul e vid, intoarce -1
//{1, 2, 3} -> 123
int stareCompusa(std::vector<int> stari);

///functie care intoace un sir de caractere
///ce contine litera cu codul ASCII 255
std::string infinit();

///functie care intoarce forma minimizata
///a automatului primit ca parametru
Automat minimizareAutomat(Automat& automat);

///operator==, intoarce true daca automatele
///sunt echivalente
bool operator==(Automat a1, Automat a2);

///functie care renumeroteaza starile automatului a2
///dupa starile automatului a1
///intoarce true daca operatia a reusit cu succes
bool renumerotareStari(Automat& a1, Automat& a2);

///functie generica, care compara doi vectori
///de acelasi tip
template<typename T>
bool comparaVectori(std::vector<T>& v1, std::vector<T>& v2)
{
    using std::pair;
    using std::vector;

    //daca vectorii au dimensiuni diferite
    //intorc false
    if (v1.size() != v2.size())
    {
        return false;
    }
    //INSPIRATIE:
    //http://www.cplusplus.com/reference/algorithm/mismatch/
    //apelam functia mismatch din biblioteca standard
    //ea intoarce o pereche cu pozitiile din vectori
    //la care elementele difera
    pair<typename vector<T>::iterator, typename vector<T>::iterator> p;
    p = std::mismatch(v1.begin(), v1.end(), v2.begin());
    //daca ambele pozitii indica sfarsitul vectorilor,
    //atunci cei doi vectori sunt egali
    if (p.first == v1.end() && p.second == v2.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}
#endif
